/*
 * File:   hardware.c
 * Author: Anton
 * comment: процедуры привязанные к конкретному контроллеру
 * Created on February 24, 2017, 9:42 PM
 */

#include <stm8s_func.h>

/**
  * @brief   Programs one byte in program or data EEPROM memory
  * @note   PointerAttr define is declared in the stm8s.h file to select if 
  *         the pointer will be declared as near (2 bytes) or far (3 bytes).
  * @param  Address : Address where the byte will be programmed
  * @param  Data : Value to be programmed
  * @retval None
  */
/**
  * @brief  Unlocks the program or data EEPROM memory
  * @param  FLASH_MemType : Memory type to unlock
  *         This parameter can be a value of @ref FLASH_MemType_TypeDef
  * @retval None
  */
union u_OPT3 OPT3;
union u_OPT4 OPT4;

void FLASH_Unlock(FLASH_MemType_TypeDef FLASH_MemType)
{
  /* Check parameter */
  //assert_param(IS_MEMORY_TYPE_OK(FLASH_MemType));
  
  /* Unlock program memory */
  if(FLASH_MemType == FLASH_MEMTYPE_PROG)
  {
    FLASH_PUKR = FLASH_RASS_KEY1;
    FLASH_PUKR = FLASH_RASS_KEY2;
  }
  /* Unlock data memory */
  else
  {
    FLASH_DUKR = FLASH_RASS_KEY2; /* Warning: keys are reversed on data memory !!! */
    FLASH_DUKR = FLASH_RASS_KEY1;
  }
}

/**
  * @brief  Locks the program or data EEPROM memory
  * @param  FLASH_MemType : Memory type
  *         This parameter can be a value of @ref FLASH_MemType_TypeDef
  * @retval None
  */
void FLASH_Lock(FLASH_MemType_TypeDef FLASH_MemType)
{
  /* Check parameter */
 // assert_param(IS_MEMORY_TYPE_OK(FLASH_MemType));
  
  /* Lock memory */
  FLASH_IAPSR &= (uint8_t)FLASH_MemType;
}

void FLASH_ProgramByte(uint32_t Address, uint8_t Data)
{
    /* Check parameters */
//    assert_param(IS_FLASH_ADDRESS_OK(Address));
    FLASH_Unlock(FLASH_MEMTYPE_DATA); //Разблокировка EEPROM
    *(__near uint8_t*) (uint16_t)Address = Data;
    FLASH_Lock(FLASH_MEMTYPE_DATA); //блокировка EEPROM
}

/**
  * @brief   Reads any byte from flash memory
  * @note   PointerAttr define is declared in the stm8s.h file to select if 
  *         the pointer will be declared as near (2 bytes) or far (3 bytes).
  * @param  Address : Address to read
  * @retval Value of the byte
  */
uint8_t FLASH_ReadByte(uint32_t Address)
{
    /* Check parameter */
 //   assert_param(IS_FLASH_ADDRESS_OK(Address));
    
    return(*(__near uint8_t *) (uint16_t)Address); 
}

void eeprom_write(uint8_t addr, uint8_t data)
{
    uint16_t real_addr;
    real_addr=addr+begin_eeprom;
    FLASH_ProgramByte(real_addr, data);
    
}
uint8_t eeprom_read(uint8_t addr)
{
    uint16_t real_addr;
    real_addr=addr+begin_eeprom;
    return FLASH_ReadByte(real_addr);
}

char *
itoa(char * buf, int val, int base)
{
	char *	cp = buf;

	if(val < 0) {
		*buf++ = '-';
		val = -val;
	}
	utoa(buf, val, base);
	return cp;
}

char *
utoa(char * buf, unsigned val, int base)
{
	unsigned	v;
	char		c;

	v = val;
	do {
		v /= base;
		buf++;
	} while(v != 0);
	*buf-- = 0;
	do {
		c = val % base;
		val /= base;
		if(c >= 10)
			c += 'A'-'0'-10;
		c += '0';
		*buf-- = c;
	} while(val != 0);
	return ++buf;
}
