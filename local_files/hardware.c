/*
 * File:   hardware.c
 * Author: Anton
 * comment: процедуры привязанные к конкретному контроллеру
 * Created on February 24, 2017, 9:42 PM
 */

#define BSET(var,bitno) ((var) |= 1<< (bitno));
#define BCLR(var,bitno) ((var) &= ~(1<< (bitno)));

#include "hardware.h"
#include <stm8s_func.h>

//параметры данного устройства
const uint8_t 
ver_h=1,                //версия прошивки (ст.)
ver_l=1,            //версия прошивки (мл.)
type_device_Hi=universal_in, //тип данного устройства - универсальное устр-во ввода
type_device_lo=0,            //подтип данного устройства - (зависит от запаяных элементов)
count_byte_answer=7;

uint8_t 
type_sensor;                 //подключенные датчики без-ти (объёмники, герконы , пожарка)

const uint16_t 
all_eeprom_addr=128; //общее кол-во памяти EEPROM в данном контроллере 
extern const uint32_t 
crystal=12000000,       //частота генератора (Гц) на первой плате - 14745600
UART1_baudrate=115200;  //скорость Уарта (бит/с) 
//-----------------------------------------------------------------

//параметры зависящие от применяемого таймера и контроллера
const uint8_t 
O_TIM_BYTE_485 = 2,     //кол-во тактов "таймера 1" на 1 байт при заданной скорости
byte_timout_tx_rx = 2;       //кол-во байт задержки между принятой посылкой и отправленной (+1,5 байта программные издержки при скорости 115200)

//описание структур
//union u_descr_port description_port[quantity_port];        //набор возможных функций для каждого порта 
//union u_descr_port ust_func_port[quantity_port];        //включенная функция выбранного порта 
//
//bool new_func_port[quantity_port];
//-----------------------------------------------
union u_info_device_slave sost_device[1];        //кол-во обслуживаемых девайсов (если ПП - 1, если ЦП то 255) 
//-----------------------------------------------
union u_CCPx_channel CCP_channel[1];

const uint8_t HSE=0xb4;          //внешний генератор
const uint8_t HSI=0xe1;          //внутренний генератор
uint16_t div_TIM1;              //делитель таймера1 для задания интервалов контроля посылок UART1
void calc_uart1_baudrate(uint16_t divider)
{
    UART1_BRR2 = (uint8_t) (((divider >> 8) & 0xF0) | (divider & 0x0F));//порядок указания регистров имеет значение!!!
    UART1_BRR1 = (uint8_t) (divider >> 4);
}

void mk_init(void)		 //ИНИЦИАЛИЗАЦИЯ
{
  //настройка тактового генератора
  CLK_ECKR_bit.HSEEN = 1;            // Включаем HSE
  CLK_SWCR_bit.SWEN=1;               // Разрешаем переключение источника тактовой частоты
  while(CLK_ECKR_bit.HSERDY != 1) {} //Ждем готовности источника тактирования
  CLK_CKDIVR = 0;                    // Предделитель равен нулю
  CLK_SWR = HSE;                    // Выбираем HSE источником тактовой частоты
  while (CLK_SWCR_bit.SWIF != 1){}   // Ждем готовности переключения 
  CLK_CCOR_bit.CCOEN=0;         //выключаем внешний выход частоты  
  CLK_ICKR=1;
  CLK_CKDIVR=0;                  //частота кварца не делиться
  CLK_SWCR_SWEN=0;               //запрешаем переключение смену ТГ
  CLK_PCKENR1=0x99;             //включаем тактирование TIM1, TIM4, UART, I2C
  CLK_PCKENR2=0x08;              //включаем тактирование АЦП

  //настраиваем таймер TIM4
  TIM4_CR1_bit.CEN=1;            //разрешаем таймер
  TIM4_PSCR=7;                   //задаём предделитель таймера 128 (число используется в последующей формуле)
  TIM4_CR1_bit.ARPE=1;           //разрешаем автолоадер (предзагрузка значения в таймер после переполнения)
  TIM4_ARR=(uint8_t)(crystal/128000);//предустановка таймера для получения нужных интервалов времени (1мсек)-(не сколько загрузит, а сколько считать))
  TIM4_IER_bit.UIE=1;            //разрешаем прерывания от таймера

  //настраиваем порты
//  PD_DDR_bit.DDR0 = 1;          // Вывод PD0 конфигурируется на вывод (out s/v mig discovery)
//  PD_CR1_bit.C10 = 1;           // Выход типа Push-pull
//  PD_CR2_bit.C20 = 0;           // Скорость переключения - до 2 МГц.

  PD_DDR_bit.DDR4 = 1;          // Вывод PD4 конфигурируется на вывод (napr_485)
  PD_CR1_bit.C14 = 1;           // Выход типа Push-pull
  PD_CR2_bit.C24 = 0;           // Скорость переключения - до 2 МГц.

  PD_DDR_bit.DDR5 = 0;          // Вывод PD5 конфигурируется на ввод (TX)
  PD_CR1_bit.C15 = 1;           // включаем подтяжку к плюсу
  PD_CR2_bit.C25 = 0;           // внешнее прерывание выключено

  PD_DDR_bit.DDR6 = 0;          // Вывод PD6 конфигурируется на ввод (RX)
  PD_CR1_bit.C16 = 1;           // включаем подтяжку к плюсу
  PD_CR2_bit.C26 = 0;           // внешнее прерывание выключено

  PB_DDR_bit.DDR4 = 0;          // Вывод PB4 конфигурируется на ввод (I2C SCL)
  PB_CR1_bit.C14 = 1;           // включаем подтяжку к плюсу
  PB_CR2_bit.C24 = 0;           // внешнее прерывание выключено

  PB_DDR_bit.DDR5 = 0;          // Вывод PB5 конфигурируется на ввод (I2C SDA)
  PB_CR1_bit.C15 = 1;           // включаем подтяжку к плюсу
  PB_CR2_bit.C25 = 0;           // внешнее прерывание выключено

  PC_DDR_bit.DDR7 = 0;          // Вывод PС7 конфигурируется на ввод (кнопка адреса)
  PC_CR1_bit.C17 = 1;           // включаем подтяжку к плюсу
  PC_CR2_bit.C27 = 0;           // внешнее прерывание выключено

  PC_DDR_bit.DDR3 = 1;          // Вывод PС3 конфигурируется на вывод (I/O Dig4)
  PC_CR1_bit.C13 = 1;           // Выход типа Push-pull
  PC_CR2_bit.C23 = 0;           // Скорость переключения - до 2 МГц.

  PA_DDR_bit.DDR3 = 1;          // Вывод PA3 конфигурируется на вывод (I/O Dig5)
  PA_CR1_bit.C13 = 1;           // Выход типа Push-pull
  PA_CR2_bit.C23 = 0;           // Скорость переключения - до 2 МГц.

  PC_DDR_bit.DDR5 = 1;          // Вывод PС5 конфигурируется на вывод (зелёный светик)
  PC_CR1_bit.C15 = 1;           // Выход типа Push-pull
  PC_CR2_bit.C25 = 0;           // Скорость переключения - до 2 МГц.

  PC_DDR_bit.DDR6 = 1;          // Вывод PС6 конфигурируется на вывод (красный светик)
  PC_CR1_bit.C16 = 1;           // Выход типа Push-pull
  PC_CR2_bit.C26 = 0;           // Скорость переключения - до 2 МГц.

 //настраиваем UART
  calc_uart1_baudrate(crystal/UART1_baudrate);  //перевод делителя в коэффициенты для указания скорости уарт1
 
  UART1_CR1_bit.UART0=0;     
  UART1_CR1_bit.M=0;            //8-и битный режим передачи
  UART1_CR1_bit.WAKE=0;     
  UART1_CR1_bit.PCEN=0;     
  UART1_CR1_bit.PS=0;     
  UART1_CR1_bit.PIEN=0;

  UART1_CR2_bit.ILIEN=0;     
  UART1_CR2_bit.RIEN=1;         //вкл. прерывание на приём данных
  UART1_CR2_bit.RWU=0;
  UART1_CR2_bit.SBK=0;
  UART1_CR2_bit.TCIEN=0;
  UART1_CR2_bit.TIEN=0;

  UART1_CR3=0;
  UART1_CR4=0;
  UART1_CR5=0;
  
//настройка таймера TIM1 для контроля работы UART  
  div_TIM1=(uint16_t)(crystal/UART1_baudrate*10/O_TIM_BYTE_485)+1;
  TIM1_PSCRH=(uint8_t) (div_TIM1 >> 8); //уст. делитель таймера 694+1 (16000000/115200*10/2) (16: частота генератора, 115200: текущая скорость обмена, 
  TIM1_PSCRL=(uint8_t) (div_TIM1);//10: бит в байте (включая служебные), 2: 2 такта таймера в одном байте  1: требуется добавить по даташиту)
                                  //ограничение на размер посылки по таймеру составляет 128 байт, по CRC8 - 15байт 
  TIM1_CR1_bit.OPM=1;             //таймер считает 1 раз и отключается
  TIM1_EGR_bit.UG=1;              //генерим программное прерывание чтобы считалось значение предделителя
  TIM1_CR1_bit.URS=1;             // только переполнение счетчика (точнее совпадение с ARR) будет приводить к прерыванию
  TIM1_ARRH=0;

//настройка АЦП
  ADC1_DeInit();
  ADC1_Init(ADC1_CONVERSIONMODE_SINGLE, ADC1_CHANNEL_2, ADC1_PRESSEL_FCPU_D8,
            ADC1_EXTTRIG_TIM,DISABLE, ADC1_ALIGN_LEFT,  ADC1_SCHMITTTRIG_CHANNEL2, DISABLE);
  ADC1_Init(ADC1_CONVERSIONMODE_SINGLE, ADC1_CHANNEL_3, ADC1_PRESSEL_FCPU_D8,
            ADC1_EXTTRIG_TIM,DISABLE, ADC1_ALIGN_LEFT,  ADC1_SCHMITTTRIG_CHANNEL3, DISABLE);
  ADC1_Init(ADC1_CONVERSIONMODE_SINGLE, ADC1_CHANNEL_4, ADC1_PRESSEL_FCPU_D8,
            ADC1_EXTTRIG_TIM,DISABLE, ADC1_ALIGN_LEFT,  ADC1_SCHMITTTRIG_CHANNEL4, DISABLE);
  ADC1_ITConfig(ADC1_IT_EOCIE ,DISABLE);
 
  PC_DDR_bit.DDR4 = 0;          // Вывод PС4 конфигурируется на вход АЦП (OUT3)
  PC_CR1_bit.C14 = 0;           // Выключаем подтяжку
  PC_CR2_bit.C24 = 0;           // внешнее прерывание выключено

  PD_DDR_bit.DDR2 = 0;          // Вывод PD2 конфигурируется на вход АЦП (OUT2) 
  PD_CR1_bit.C12 = 0;           // Выключаем подтяжку
  PD_CR2_bit.C22 = 0;           // внешнее прерывание выключено

  PD_DDR_bit.DDR3 = 0;          // Вывод PD3 конфигурируется на вход АЦП  (OUT1)
  PD_CR1_bit.C13 = 0;           // Выключаем подтяжку
  PD_CR2_bit.C23 = 0;           // внешнее прерывание выключено
  
  //настройка датчика температуры LM75
  i2c_master_init(crystal,100000);

  type_sensor=eeprom_read(O_type_sensor);
  
  sost_device[MY_ADRESS].PART.ver_h=ver_h;
  sost_device[MY_ADRESS].PART.ver_l=ver_l;
  sost_device[MY_ADRESS].PART.type_device_Hi=type_device_Hi;
  sost_device[MY_ADRESS].PART.type_device_lo=type_device_lo;
  sost_device[MY_ADRESS].PART.type_sensor=type_sensor;
  sost_device[MY_ADRESS].PART.speed_request=high;
  sost_device[MY_ADRESS].PART.count_byte_answer=count_byte_answer;

 __enable_interrupt();           //разрешаем глобальные прерывания(ассемблерная команда RIM)

    eeprom_write(O_dev_adress,0x06);//пробный адрес в сети
}


//-----------------------------------------------------
void upr_485(bool napravlenie)
{
    PIN_NAPR_485=napravlenie;
}
//-----------------------------------------------------
void upr_out(uint8_t change_num_out, uint8_t state_out)          //переключение выходов D4 и D5 (по схеме)
{
union u_state_all_output state_all_output; 
union u_state_all_output state_all_change;

  state_all_change.ALL=change_num_out;
  state_all_output.ALL=state_out;

  if(state_all_change.PART.out4==1)
  {
    PIN_OUT_D4=state_all_output.PART.out4;
  }
}
