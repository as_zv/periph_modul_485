#include "my_key_press.h"

bool key_press (uint8_t num_kn)
{
    bool ret_out=0;
    switch (num_kn)
    {
        case 1:
        {
            ret_out=!KN_ADRESS;
            break;
        }
        default:
        {
            ret_out=false;
            break;
        }
            
    }
    return ret_out;
}
