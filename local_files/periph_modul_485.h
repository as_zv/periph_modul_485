/* файл описания основных функций и переменных в программе
 * File:   periph_modul_485.h
 * comment: программа управления модулем с универсальным входами для умного дома по линии 485
 * Author: Anton
 * Created on: декабрь 2017 г.
*/
#ifndef PERIPH_MODUL_485_H
#define	PERIPH_MODUL_485_H

#include <stdio.h>
#include "stdbool.h"
#include <stdint.h>

#include "short_func.h" //описания коротких функций выполняемых по командам управления
#include "hardware.h"   //константы и функции привязанные к конкретному контроллеру
#include "tx_rx.h"      //процедуры приёма и передачи данных
#include "common_func.h"//файл описания общих функций
#include "interrupt.h"  //файл описания прерываний
#include "my_klava.h"     //обработка кнопок
#include "my_key_press.h"//проверка выводов с кнопок и занесение в массив хранящий состояние кнопок
#include <stm8s_adc1.h>
#include "lm75.h"
#include "flood_I2C_master.h"

#define ADC_no_change 0
#define ADC_KZ 1
#define ADC_change 2
#define ADC_open 3

//============================================================================
extern int16_t
LM75_Temp;              //закодированное значение температуры от LM75

extern bool ok;                //получены корректные данные с датчика температуры
//extern bool over_time_I2C;    //флаг контроля времени ответа по I2C
extern uint8_t
Humidity,               //влажность воздуха с датчика
TempLo,            //младший байт температуры с датчика
TempHi;            //старший байт

//объединения
typedef struct st_I2C
{
	uint8_t  
        LM75_online    :1,      //подключен датчик температуры LM75
	SHT30_online   :1,      //подключен датчик температуры и влажности SHT30
	temp           :6;      //резерв
}st_I2C;
union u_st_I2C
{
	uint8_t ALL;
	st_I2C PART;
};

typedef struct state_all_ADC
{
        uint8_t
	KZ_chan1        :1,   //КЗ на линии 1 канала 
	open_chan1      :1,   //обрыв на линии 1 канала 
        change_chan1    :1,   //изменение напряжения отн-но установленного 1 канала  
        reserv_chan1    :1,   //резерв 1 канала 
	KZ_chan2        :1,   //КЗ на линии 2 канала 
	open_chan2      :1,   //обрыв на линии 2 канала 
        change_chan2    :1,   //изменение напряжения отн-но установленного 2 канала  
        reserv_chan2    :1;   //резерв 2 канала 
}state_all_ADC;
union u_state_all_ADC
{
	uint8_t ALL;
	state_all_ADC PART;
};

typedef struct state_IN
{
        uint8_t
	KZ              :1,   //КЗ на линии
	open            :1,   //обрыв на линии
        change          :1,   //изменение напряжения отн-но установленного 
        reserv          :1,
        temp            :4;   //резерв
}state_IN;
union u_state_IN
{
	uint8_t ALL;
	state_IN PART;
};
//======================================================================

void mk_init(void);                 //ИНИЦИАЛИЗАЦИЯ
void rx_stat_sv (void);         	//приём состояния светильников
void form_current_state (void);     //передача своего состояния - запрашивающему
void ctrl_timers(void);             //проверка таймеров
void short_press_adress (void);     //обработка обычного нажатия кнопки адрес
void long_press_adress (void);     //обработка длинного нажатия кнопки адрес
void ctrl_klava (void);             //проверка кнопок
void med_V_I(void);         	        //подпрограмма усреднения выбр каналов
void ctrl_IN (uint8_t num_IN, uint8_t ADC_IN);  //формирование состояния входов датчиков в зав-ти от напряжения на входе  
void stable_state_IN(uint8_t num_IN);        //ожидание стабилизации состояния 
void output_state_formation(uint8_t num_IN);     //формирование выходного байта с состоянием всех входов АЦП
void save_value_ADC_EEPROM (void);       //команда сохранения значений с АЦП
void calc_deviation_ADC (void);       //расчёт значений отклонений на АЦП

//КОНСТАНТЫ

#endif	/* PERIPH_MODUL_485.H */
