/* 
 * File:   
 * Author: Zuev Anton
 * Comments: файл описания констант и функций привязанных к конкретному контроллеру и девайсу
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef HARDWARE_H
#define	HARDWARE_H

#include <iostm8s003f3.h>       //не менять на общий файл !!! (т.к. он только под stm8s208)
#include <intrinsics.h>
#include <stm8s_func.h>
#include <stm8s_adc1.h>
#include "lm75.h"
#include "flood_I2C_master.h"

#include "stdint.h"
#include "stdio.h"
#include "stdbool.h"

#include "short_func.h" //описания коротких функций выполняемых по командам управления
//#include "common_func.h" 

#define RX1_FERR UART1_SR_bit.FE // ошибка кадра
#define RX1_ERR UART1_SR_bit.OR_LHE //ошибка переполения буфера
#define RX1_NERR UART1_SR_bit.NF // шум на линии
#define RX1_ON  UART1_CR2_bit.REN//включение приёмника
#define RX1_IE  UART1_CR2_bit.RIEN//включение прервания на приём
#define RX1_IF  UART1_SR_bit.RXNE//принят байт из линии УАРТ
#define RX1_BUF  UART1_DR       //аппаратный приёмный буфер

#define TX1_ON  UART1_CR2_bit.TEN//включение передатчика
#define TX1_IF  UART1_SR_bit.TXE //бит готовности буфера принять след. байт для передачи в линию УАРТ 
#define TX1_BUF UART1_DR        //аппаратный передающий буфер
#define TX1_END UART1_SR_bit.TC //окончание передачи (из буфера)

#define TMR1_END TIM1_ARRL      //байт сравнения таймера с заданным значением для выполения прерывания
#define TMR1_ON TIM1_CR1_bit.CEN//включение таймера
#define TMR1_Hi TIM1_CNTRH      //старший байт таймера1
#define TMR1_Lo TIM1_CNTRL      //младший байт таймера1
#define TMR1_IF TIM1_SR1_bit.UIF//прерывание от таймера1
#define TMR1_IE TIM1_IER_bit.UIE//включение прерывания
//====================================================================
//типы устройств
#define gate 1          //шлюз
#define universal_in 10 //устр-во с универсальными входами
#define flood_water  11 //контроллер протечек
#define ctrl_heating 12 //устр-во управления отоплением
//====================================================================
//требуемая скорость опроса
#define low 0       //скорость опроса данного устройства должна быть низкой
#define high 1      //скорость опроса данного устройства должна быть высокой
//-----------------------------------------------------------------
//параметры данного устройства
extern const uint8_t 
ver_h,            //версия прошивки (ст.)
ver_l,            //версия прошивки (мл.)
count_byte_answer,//кол-во байт в ответ на запрос состояния
type_device_Hi,   //тип данного устройства (главный класс устр-ва - универс. устр-во, протечка, отопление)
type_device_lo; //подтип данного устройства (подкласс устр-ва - что распаяно на плате: темп, темп+влаж, освещённость, входы без-ти и выходы реле)

extern uint8_t 
type_sensor;    //тип подключенных датчиков безопасности (объёмники, герконы, пожарка и пр.)
#define quantity_kn 2   //кол-во используемых кнопок (число должно быть на 1 ед. больше чем реальное кол-во кнопок)     
extern const uint16_t all_eeprom_addr; //общее кол-во памяти EEPROM в данном контроллере 

extern const uint32_t 
crystal,       //частота генератора (Гц)
UART1_baudrate;  //скорость Уарта (бит/с) 
//-----------------------------------------------------------------
//описание констант
extern const uint8_t 
CCP1_channel, //
CCP2_channel, //
CCP3_channel, //
CCP4_channel, //
TMR2_channel, //
TMR4_channel, //
TMR6_channel; //
//---------------------------------------------------------------
extern const uint8_t
byte_timout_tx_rx,       //кол-во байт задержки между принятой посылкой и отправленной (+1,5 байта программные издержки при скорости 115200)
O_TIM_BYTE_485;         //кол-во тактов "таймера 1" на 1 байт при заданной скорости

//переменные
extern uint8_t
OUT_BCD[3];     //массив преобразованного дес-го числа  
//---------------------------------------------------------------
//описание структур
typedef struct descr_port
{
	unsigned in   :1;         //возможность работать как вход
    unsigned out  :1;         //возможность работать как выход
    unsigned adc  :1;         //возможность работать как АЦП
    unsigned dac  :1;         //возможность работать как ЦАП
    unsigned pwm  :1;         //возможность работать как ШИМ
    unsigned i2c  :1;         //возможность работать как I2C
    unsigned spi  :1;         //возможность работать как SPI
    unsigned spec :1;         //есть возможность вкл. спец. функции
}tdescr_port;
union u_descr_port
{
	uint8_t ALL;
	tdescr_port PART;
};
extern union u_descr_port description_port[];        //набор возможных функций для каждого порта 
extern union u_descr_port ust_func_port[];        //включенная функция выбранного порта 

extern bool new_func_port[];
//-----------------------------------------------
typedef struct info_device_slave
{
    unsigned 		        :1;  //резерв
    unsigned speed_request  :1;  //требуемая скорость опроса
    unsigned rx_err               :1;  //результат последнего сеанса связи
    unsigned temp                 :5; //резерв
    uint8_t type_device_Hi;        //тип данного устройства (главный класс устр-ва - универс. устр-во, протечка, отопление)
    uint8_t type_device_lo;        //подтип данного устройства (подкласс устр-ва - что распаяно на плате: темп, темп+влаж, освещённость, входы без-ти и выходы реле)
    uint8_t type_sensor;            //тип подключенных датчиков безопасности (объёмники, герконы, пожарка и пр.)
    uint8_t ver_h;                      //версия устройства (ст.)
    uint8_t ver_l;                      	//версия устройства (мл.)
    uint8_t count_byte_answer;//кол-во байт полезной инфы передаваемой в ответе на запрос состояния
    uint16_t count_rx_err;        //счётчик ошибок приёма данных от мастера (считается и храниться слэйвом)
}info_device_slave;

union u_info_device_slave
{
	info_device_slave PART;
};
extern union u_info_device_slave sost_device[];        //кол-во обслуживаемых девайсов (если ПП - 1, если ЦП то 255) 
//-----------------------------------------------
typedef struct CCPx_channel
{
    uint32_t ust_freq_PWM   ;         //установленная частота ШИМ (в герцах)
    uint8_t ust_percent_PWM ;         //установленная длительность ШИМ (в процентах)
    uint8_t TMR_channel     ;         //номер задействованного таймера для выбранного канала ШИМ
    unsigned TMR_PWM_ready  :1;       //таймер выбран корректно
    unsigned FREQ_PWM_ready :1;       //выбранная частота находится допустимом диапазоне
    unsigned duty_PWM_ready :1;       //выбранная скважность находиться в допустимом диапазоне
    unsigned PWM_ON         :1;       //выбранный канал ШИМ включен
}tCCPx_channel;
union u_CCPx_channel
{
	unsigned ALL;
	tCCPx_channel PART;
};
extern union u_CCPx_channel CCP_channel[];
//-----------------------------------------------
typedef struct state_all_output
{
        uint8_t
	out1      :1,   //состояние выхода 1
	out2      :1,   //состояние выхода 2
	out3      :1,   //состояние выхода 3
	out4      :1,   //состояние выхода 4
	out5      :1,   //состояние выхода 5
	out6      :1,   //состояние выхода 6
	out7      :1,   //состояние выхода 7
	out8      :1;   //состояние выхода 8
}state_all_output;
union u_state_all_output
{
	uint8_t ALL;
	state_all_output PART;
};
 
//=====================================================================
//описание функций
void upr_vkl_sv(bool OUT);          //управление выходом RC0
void upr_485(bool napravlenie);     //переключение направление передачи по 485 порту
void f_descript_port(void);             //задание возможных режимов работы портов выбранного устройства
void upr_out(uint8_t change_num_out, uint8_t state_out);          //переключение выходов D4 и D5 (по схеме)
//============================================================
//распиновыка выводов
#define PIN_NAPR_485 PD_ODR_bit.ODR4  //выход управления направлением передачи данных по 485
#define KN_ADRESS PC_IDR_bit.IDR7     //кнопка включения режима получения адреса
                                      //(кор. наж. - получение адреса(только,если свой адрес уже сброшен), длит. нажатие - сброс собств. адреса)
#define PIN_SV_GREEN PC_ODR_bit.ODR5  //зелёный светик, 
#define PIN_SV_RED PC_ODR_bit.ODR6    //красный светик, режимы работы:
                            //зел. горит/красный не горит - устройство в норм режиме работы (адрес получен)
                            //зел. горит/красный мигает - устройство в норм режиме работы, но есть ошибки передачи данных
                            //зел. горит/красный горит - устройство прошивается
                            //зел. не горит/красный не горит - устройство не запустилось
                            //зел. не горит/красный мигает - нажата кнопка ожидания адреса от ЦП
                            //зел. не горит/красный горит - устройство перешло в режим тишины (для прошивки)
                            //зел. мигает/красный не горит- включен режим идентификации устройства 
                            //зел. мигает/красный мигает - устройство включилось, но адрес не получен
                            //зел. мигает/красный горит - 
#define PIN_OUT_D4 PC_ODR_bit.ODR3  // управление выходом D4
#define PIN_OUT_D5 PA_ODR_bit.ODR3  // управление выходом D5
//==================================================
#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* HARDWARE_H */

