/* 
 * File:   
 * Author: Zuev Anton
 * Comments: описание используемых аппаратных функций в контроллере STM8
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef STM8S_FUNC_H
#define	STM8S_FUNC_H

#include <iostm8s003f3.h>
#include <intrinsics.h>
#include <stm8s_func.h>

#include "stdint.h"
#include "stdio.h"
#include "stdbool.h"
#include "stdlib.h"


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Uncomment the line below to expanse the "assert_param" macro in the
   Standard Peripheral Library drivers code */
 //#define USE_FULL_ASSERT    (1) */

/* Exported macro ------------------------------------------------------------*/
#ifdef  USE_FULL_ASSERT

/**
  * @brief  The assert_param macro is used for function's parameters check.
  * @param expr: If expr is false, it calls assert_failed function
  *   which reports the name of the source file and the source
  *   line number of the call that failed.
  *   If expr is true, it returns no value.
  * @retval : None
  */
#define assert_param(expr) ((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__))
/* Exported functions ------------------------------------------------------- */
void assert_failed(uint8_t* file, uint32_t line);
#else
#define assert_param(expr) ((void)0)
#endif /* USE_FULL_ASSERT */

#define STM8S003
#define FLASH_PROG_START_PHYSICAL_ADDRESS ((uint32_t)0x008000) /*!< Program memory: start address */
#define begin_eeprom 0x4000     //начало еепромки для stm8s003 

#if defined(STM8S103) || defined(STM8S003) || defined(STM8S903) || defined(STM8AF622x)
 #define FLASH_PROG_END_PHYSICAL_ADDRESS   ((uint32_t)0x9FFF)   /*!< Program memory: end address */
 #define FLASH_PROG_BLOCKS_NUMBER          ((uint16_t)128)      /*!< Program memory: total number of blocks */
 #define FLASH_DATA_START_PHYSICAL_ADDRESS ((uint32_t)0x004000) /*!< Data EEPROM memory: start address */
 #define FLASH_DATA_END_PHYSICAL_ADDRESS   ((uint32_t)0x00427F) /*!< Data EEPROM memory: end address */
 #define FLASH_DATA_BLOCKS_NUMBER          ((uint16_t)10)       /*!< Data EEPROM memory: total number of blocks */
 #define FLASH_BLOCK_SIZE                  ((uint8_t)64)        /*!< Number of bytes in a block (common for Program and Data memories) */
#endif /* STM8S103 or STM8S003 or STM8S903 or STM8AF622x*/

#define FLASH_RASS_KEY1 ((uint8_t)0x56) /*!< First RASS key */
#define FLASH_RASS_KEY2 ((uint8_t)0xAE) /*!< Second RASS key */

#define OPTION_BYTE_START_PHYSICAL_ADDRESS  ((uint16_t)0x4800)
#define OPTION_BYTE_END_PHYSICAL_ADDRESS    ((uint16_t)0x487F)
#define FLASH_OPTIONBYTE_ERROR              ((uint16_t)0x5555) /*!< Error code option byte 
                                                                   (if value read is not equal to complement value read) */

//========================================================
typedef struct OPT3_BIT         //служебный регистр - пишется во флэш (через спецфункцию)
{
        uint8_t
	WWDG_HALT   :1,   //
	WWDG_HW     :1,   //
	IWDG_HW     :1,   //
	LSI_EN      :1,   //включение внутреннего генератора на 128кГц
	HSI_TRIM    :1,   //
	reserv      :3;   //
}OPT3_BIT;
union u_OPT3
{
	uint8_t ALL;
	OPT3_BIT PART;
};
extern union u_OPT3 OPT3;

//-----------------------------------------------
typedef struct OPT4_BIT         //служебный регистр - пишется во флэш (через спецфункцию)
{
        uint8_t
	PRSC0       :1,   //
	PRSC1       :1,   //
	CKAWUSEL    :1,   //переключение внутренней логики на генератор на 128кГц
	EXTCLK      :1,   //
	reserv      :4;   //
}OPT4_BIT;
union u_OPT4
{
	uint8_t ALL;
	OPT4_BIT PART;
};
extern union u_OPT4 OPT4;
//-----------------------------------------------

/**
 * IO definitions
 *
 * define access restrictions to peripheral registers
 */
#define     __I     volatile const   /*!< defines 'read only' permissions     */
#define     __O     volatile         /*!< defines 'write only' permissions    */
#define     __IO    volatile         /*!< defines 'read / write' permissions  */

/*!< STM8 Standard Peripheral Library old types (maintained for legacy purpose) */

typedef int32_t  s32;
typedef int16_t s16;
typedef int8_t  s8;

typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef enum {RESET = 0, SET = !RESET} FlagStatus, ITStatus, BitStatus, BitAction;

typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
#define IS_FUNCTIONALSTATE_OK(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))

typedef enum {ERROR = 0, SUCCESS = !ERROR} ErrorStatus;

/**
  * @brief  Macro used by the assert function in order to check the different sensitivity values for the data eeprom and flash program Address
  */
#define IS_FLASH_ADDRESS_OK(ADDRESS)((((ADDRESS) >= FLASH_PROG_START_PHYSICAL_ADDRESS) && ((ADDRESS) <= FLASH_PROG_END_PHYSICAL_ADDRESS)) || \
                                     (((ADDRESS) >= FLASH_DATA_START_PHYSICAL_ADDRESS) && ((ADDRESS) <= FLASH_DATA_END_PHYSICAL_ADDRESS)))

/**
  * @brief  FLASH Memory types
  */
typedef enum {
    FLASH_MEMTYPE_PROG      = (uint8_t)0xFD, /*!< Program memory */
    FLASH_MEMTYPE_DATA      = (uint8_t)0xF7  /*!< Data EEPROM memory */
} FLASH_MemType_TypeDef;

/******************************************************************************/
/*                          Peripherals Base Address                          */
/******************************************************************************/

/** @addtogroup MAP_FILE_Base_Addresses
  * @{
  */
#define OPT_BaseAddress         0x4800
#define GPIOA_BaseAddress       0x5000
#define GPIOB_BaseAddress       0x5005
#define GPIOC_BaseAddress       0x500A
#define GPIOD_BaseAddress       0x500F
#define GPIOE_BaseAddress       0x5014
#define GPIOF_BaseAddress       0x5019
#define GPIOG_BaseAddress       0x501E
#define GPIOH_BaseAddress       0x5023
#define GPIOI_BaseAddress       0x5028
#define FLASH_BaseAddress       0x505A
#define EXTI_BaseAddress        0x50A0
#define RST_BaseAddress         0x50B3
#define CLK_BaseAddress         0x50C0
#define WWDG_BaseAddress        0x50D1
#define IWDG_BaseAddress        0x50E0
#define AWU_BaseAddress         0x50F0
#define BEEP_BaseAddress        0x50F3
#define SPI_BaseAddress         0x5200
#define I2C_BaseAddress         0x5210
#define UART1_BaseAddress       0x5230
#define UART2_BaseAddress       0x5240
#define UART3_BaseAddress       0x5240
#define TIM1_BaseAddress        0x5250
#define TIM2_BaseAddress        0x5300
#define TIM3_BaseAddress        0x5320
#define TIM4_BaseAddress        0x5340
#define TIM5_BaseAddress        0x5300
#define TIM6_BaseAddress        0x5340
#define ADC1_BaseAddress        0x53E0
#define ADC2_BaseAddress        0x5400
#define CAN_BaseAddress         0x5420
#define CFG_BaseAddress         0x7F60
#define ITC_BaseAddress         0x7F70
#define DM_BaseAddress          0x7F90

/**
  * @}
  */

#define ADC1 ((ADC1_TypeDef *) ADC1_BaseAddress)

/**
  * @brief  Analog to Digital Converter (ADC1)
  */
 typedef struct ADC1_struct
 {
  __IO uint8_t DB0RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB0RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  __IO uint8_t DB1RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB1RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  __IO uint8_t DB2RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB2RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  __IO uint8_t DB3RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB3RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  __IO uint8_t DB4RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB4RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  __IO uint8_t DB5RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB5RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  __IO uint8_t DB6RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB6RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  __IO uint8_t DB7RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB7RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  __IO uint8_t DB8RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB8RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  __IO uint8_t DB9RH;         /*!< ADC1 Data Buffer Register (MSB)  */
  __IO uint8_t DB9RL;         /*!< ADC1 Data Buffer Register (LSB)  */
  uint8_t RESERVED[12];       /*!< Reserved byte */
  __IO uint8_t CSR;           /*!< ADC1 control status register */
  __IO uint8_t CR1;           /*!< ADC1 configuration register 1 */
  __IO uint8_t CR2;           /*!< ADC1 configuration register 2 */
  __IO uint8_t CR3;           /*!< ADC1 configuration register 3  */
  __IO uint8_t DRH;           /*!< ADC1 Data high */
  __IO uint8_t DRL;           /*!< ADC1 Data low */
  __IO uint8_t TDRH;          /*!< ADC1 Schmitt trigger disable register high */
  __IO uint8_t TDRL;          /*!< ADC1 Schmitt trigger disable register low */
  __IO uint8_t HTRH;          /*!< ADC1 high threshold register High*/
  __IO uint8_t HTRL;          /*!< ADC1 high threshold register Low*/
  __IO uint8_t LTRH;          /*!< ADC1 low threshold register high */
  __IO uint8_t LTRL;          /*!< ADC1 low threshold register low */
  __IO uint8_t AWSRH;         /*!< ADC1 watchdog status register high */
  __IO uint8_t AWSRL;         /*!< ADC1 watchdog status register low */
  __IO uint8_t AWCRH;         /*!< ADC1 watchdog control register high */
  __IO uint8_t AWCRL;         /*!< ADC1 watchdog control register low */
 }
 ADC1_TypeDef;

/** @addtogroup ADC1_Registers_Reset_Value
  * @{
  */
 #define  ADC1_CSR_RESET_VALUE    ((uint8_t)0x00)
 #define  ADC1_CR1_RESET_VALUE    ((uint8_t)0x00)
 #define  ADC1_CR2_RESET_VALUE    ((uint8_t)0x00)
 #define  ADC1_CR3_RESET_VALUE    ((uint8_t)0x00)
 #define  ADC1_TDRL_RESET_VALUE   ((uint8_t)0x00)
 #define  ADC1_TDRH_RESET_VALUE   ((uint8_t)0x00)
 #define  ADC1_HTRL_RESET_VALUE   ((uint8_t)0x03)
 #define  ADC1_HTRH_RESET_VALUE   ((uint8_t)0xFF)
 #define  ADC1_LTRH_RESET_VALUE   ((uint8_t)0x00)
 #define  ADC1_LTRL_RESET_VALUE   ((uint8_t)0x00)
 #define  ADC1_AWCRH_RESET_VALUE  ((uint8_t)0x00)
 #define  ADC1_AWCRL_RESET_VALUE  ((uint8_t)0x00)
/**
  * @}
  */

/** @addtogroup ADC1_Registers_Bits_Definition
  * @{
  */
 #define ADC1_CSR_EOC     ((uint8_t)0x80) /*!< End of Conversion mask */
 #define ADC1_CSR_AWD     ((uint8_t)0x40) /*!< Analog Watch Dog Status mask */
 #define ADC1_CSR_EOCIE   ((uint8_t)0x20) /*!< Interrupt Enable for EOC mask */
 #define ADC1_CSR_AWDIE   ((uint8_t)0x10) /*!< Analog Watchdog interrupt enable mask */
 #define ADC1_CSR_CH      ((uint8_t)0x0F) /*!< Channel selection bits mask */

 #define ADC1_CR1_SPSEL   ((uint8_t)0x70) /*!< Prescaler selection mask */
 #define ADC1_CR1_CONT    ((uint8_t)0x02) /*!< Continuous conversion mask */
 #define ADC1_CR1_ADON    ((uint8_t)0x01) /*!< A/D Converter on/off mask */

 #define ADC1_CR2_EXTTRIG ((uint8_t)0x40) /*!< External trigger enable mask */
 #define ADC1_CR2_EXTSEL  ((uint8_t)0x30) /*!< External event selection mask */
 #define ADC1_CR2_ALIGN   ((uint8_t)0x08) /*!< Data Alignment mask */
 #define ADC1_CR2_SCAN    ((uint8_t)0x02) /*!< Scan mode mask */

 #define ADC1_CR3_DBUF    ((uint8_t)0x80) /*!< Data Buffer Enable mask */
 #define ADC1_CR3_OVR     ((uint8_t)0x40) /*!< Overrun Status Flag mask */

/**
  * @}
  */


void FLASH_ProgramByte(uint32_t Address, uint8_t Data);
uint8_t FLASH_ReadByte(uint32_t Address);
void eeprom_write(uint8_t addr, uint8_t data);
uint8_t eeprom_read(uint8_t addr);
void FLASH_Unlock(FLASH_MemType_TypeDef FLASH_MemType);
void FLASH_Lock(FLASH_MemType_TypeDef FLASH_MemType);
extern char *	itoa(char * buf, int val, int base);
extern char *	utoa(char * buf, unsigned val, int base);

#endif	/* STM8S_FUNC_H */
