/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef INTERRUPT_H
#define	INTERRUPT_H

#include <iostm8s003f3.h>
#include <intrinsics.h>

#include <stdio.h>
#include <stdlib.h>

#include "tx_rx.h" 
#include "common_func.h"
//void interrupt high_isr(void);

//переменные
volatile extern uint16_t COUNT_RX_BUF_ERR;     //счётчик ошибок переполнения буфера приёмника
extern bool over_time_I2C;

//КОНСТАНТЫ
extern const uint8_t 
O_TMR_NEW_DIGIT,//константа для переключения на след разряд индикатора (5мсек)
O_TMR_1MSEC,  //константа для получения интервалов 1мсек
O_TMR_10MSEC,  //константа для получения интервалов 10мсек
O_TMR_100MSEC,  //константа для получения интервалов 100мсек
O_TMR_500MSEC;  //константа для получения интервалов 500мсек
extern const uint16_t O_TMR_SEC;  //константа для получения интервалов 1сек
#endif	/* XC_HEADER_TEMPLATE_H */

