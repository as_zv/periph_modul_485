/* File:   periph_modul_485.с
 * comment: программа периферийного модуля для сети 485 (умного дома) 
 * имеет 2 универсальных входа, датчик света, температуры и влажности
 * Author: Anton
 * Created on: декабрь 2017 г.
*/
#include "periph_modul_485.h"	//файл описания основных функций и переменных относящихся к этой программе

//переменные
uint16_t
ADC_OUT[3];   //входы АЦП 1...3

int16_t
LM75_Temp;              //закодированное значение температуры от LM75

bool ok;                //получены корректные данные с датчика температуры

uint8_t
ADC_change_up[3],       //значение с АЦП, выше которого фиксируется изменение   
ADC_change_down[3],     //значение с АЦП, ниже которого фиксируется изменение

Humidity,               //влажность воздуха с датчика
Temp_Hg[6],            //байты температуры и влажности с датчиков (без обработки)
count_izmen_input[3];//счётчик изменений зафиксированных состояний

//объединения
union u_state_IN actual_state_IN[3];//зафиксированное стабильное состояние входа
union u_state_IN current_state_IN[3];//текущее состояние входа 
union u_state_IN buffer_state_IN[3];//буфер для сравнения предыдущего и текущего состояние входа 
union u_state_all_ADC current_state_input_ADC;//состояние входов АЦП - по 2 бита на вход (0 - нет изменений, 1 - КЗ, 2 - есть изменения, 3 - вход не подключен)
//union u_state_all_ADC old_state_input_ADC;
union u_st_I2C state_I2C;

//КОНСТАНТЫ
const uint8_t
ADC_open_up=0xFF,           //верхний порог состояния входа АЦП считающегося обрывом
ADC_open_down=0xFA,         //нижний порог состояния входа АЦП считающегося обрывом
ADC_KZ_down=0,              //нижний порог состояния входа АЦП считающегося КЗ
ADC_KZ_up=0x10,             //верхний порог состояния входа АЦП считающегося КЗ

O_stable_sost=5,        //кол-во считанных подряд одинаковых значений состояния входа
O_deviation=5;          //отклонение от уст. значения на АЦП
//==========================================================================
void main(void)			//главный цикл
{
    uint8_t bufer_priema;       //буфер для пришедшей по сети команды
	mk_init();		//инициализация контроллера
   	RX1_ON=1;	        //включение приёмника
	upr_485(priem);		//переключение драйвера на приём
    MY_ADRESS=eeprom_read(O_dev_adress);//чтение собственного адреса из еепромки
    ADC_OUT[0]=eeprom_read(O_ADC1);    //считать значение АЦП канала 1 из еепромки
    ADC_OUT[1]=eeprom_read(O_ADC2);    //считать значение АЦП канала 1 из еепромки
    ADC_OUT[2]=eeprom_read(O_ADC3);    //считать значение АЦП канала 1 из еепромки
    calc_deviation_ADC();               //расчёт значений отклонений на АЦП
    if(eeprom_read(O_regim_starta)==O_progr_new_firmware)
    {
        send_uart_program_success(MY_ADRESS);
    }
 
	for(;;)			//запуск бесконечного цикла
	{	
        ctrl_timers();          //проверка всех таймеров
        bufer_priema=ctrl_priem();  //приём команд из линии
        if(bufer_priema==command_new_param)         //проверяем какая команда пришла? 
        {
            rx_stat_sv();                           //применение новых параметров работы 
        }
        else if(bufer_priema==ask_now_state)        //проверяем какая команда пришла? 
        {
            form_current_state();                   //отправка текущих параметров работы устр-ва
        }
        else if(bufer_priema==command_save_value_ADC)//проверяем какая команда пришла? 
        {
            save_value_ADC_EEPROM();                   //команда сохранения значений с АЦП
        }
	}
}

//==========================================
void form_current_state (void)       //передача своего состояния - запрашивающему
{
    uint8_t i=0;
    TX_PACKET.PART.XD_BYTE[i++]=current_state_input_ADC.ALL;
    TX_PACKET.PART.XD_BYTE[i++]=count_izmen_input[0];
    TX_PACKET.PART.XD_BYTE[i++]=count_izmen_input[1];
    TX_PACKET.PART.XD_BYTE[i++]=state_I2C.ALL;
    TX_PACKET.PART.XD_BYTE[i++]=Temp_Hg[0];
    TX_PACKET.PART.XD_BYTE[i++]=Temp_Hg[1];
    TX_PACKET.PART.XD_BYTE[i++]=Temp_Hg[3];
    TX_PACKET.PART.XD_BYTE[i++]=Temp_Hg[4];
    TX_PACKET.PART.XD_BYTE[i++]=ADC_OUT[0];
    TX_PACKET.PART.XD_BYTE[i++]=ADC_OUT[1];
    TX_PACKET.PART.XD_BYTE[i++]=ADC_OUT[2];
    form_packet(RX_PACKET.PART.FROM,answer_now_state,i);
}
//==========================================
void save_value_ADC_EEPROM (void)       //команда сохранения значений с АЦП
{
    f_short_answer_on_command(answer_save_value_ADC);
    calc_deviation_ADC();               //расчёт значений отклонений на АЦП
    eeprom_write(O_ADC1,ADC_OUT[0]);    //сохранения значений с АЦП в еепромке
    eeprom_write(O_ADC2,ADC_OUT[1]);
    eeprom_write(O_ADC3,ADC_OUT[2]);
}
//===============================================
void calc_deviation_ADC (void)       //расчёт значений отклонений на АЦП
{
    uint8_t i;
    for(i=0; i<3; i++)
    {
        if(ADC_OUT[i]<(0xFF-O_deviation))
        {
            ADC_change_up[i]=(ADC_OUT[i]+O_deviation);
        }
        else
            ADC_change_up[i]=0xFE;

        if(ADC_OUT[i]>(O_deviation))
        {
            ADC_change_down[i]=(ADC_OUT[i]-O_deviation);
        }
        else
            ADC_change_down[i]=1;
    }
}
//===============================================
void rx_stat_sv (void)	//приём нового состояния
{
    upr_out(RX_PACKET.PART.XD_BYTE[0],RX_PACKET.PART.XD_BYTE[1]); //переносим принятое от хаба состояние выходов на заданные порты
                                                                  //0-ой байт - требуемые для изменения выходы - побитно)
                                                                  //1-ый байт - состояние изменяемых выходов - побитно)
    f_answer_new_param();                   //отправка подтверждения принятя новых параметров
}
//=============================================================
void ctrl_timers(void)              //проверка таймеров
{
    uint8_t i;
  
    if(FL_TIM_10MSEC==true)
    {
        FL_TIM_10MSEC=false;    //сброс флага сработки таймера
        ADC_OUT[0]=(adc(4)); 	        //меряем вход I/O A/D 1
        ADC_OUT[1]=(adc(3)); 	        //меряем вход I/O A/D 2
        ADC_OUT[2]=(adc(2)); 	        //меряем вход I/O A/D 3
        med_V_I();	        //усредение выбранных измеренных значений
        ctrl_klava();           //проверка нажатых кнопок
    }
    if(FL_TIM_100MSEC==true)
    {
        FL_TIM_100MSEC=false;            //сброс флага сработки таймера
        for(i=0; i<3; i++)
        {
          ctrl_IN(i, (uint8_t)ADC_OUT[i]);//анализ состояния выбранного входа
          stable_state_IN(i);           //ожидание стабилизации состояния
          output_state_formation(i);     //формирование выходного байта с состоянием всех входов АЦП
        }
    }
    if(FL_TIM_500MSEC==true)    //прошло полсекунды
    {
        FL_TIM_500MSEC=false;
        process_svetik_common();//управляем состоянием общих для всех плат светиков
//        PD_ODR_bit.ODR0=!PD_ODR_bit.ODR0;//для дискавери
    }
    if(FL_TIM_SEC==true)    //протикала секунда?
    {
        FL_TIM_SEC=false;
        count_time();   //подсчёт текущего времени
        ctrl_silence();//проверка таймера режима тишины
        LM75_Temp = LM75_read(0, &ok); //где: 0 - смещение адреса датчика температуры отн-но начального, ok - датчик ответил
        if(ok==true)
        {
            state_I2C.PART.LM75_online=true;
            Temp_Hg[1]=(uint8_t)(LM75_Temp);
            Temp_Hg[0]=(uint8_t)(LM75_Temp>>8);
        }
//        if(i2c_rd_reg(0x91,0,0,&Temp_Hg[0],2)==I2C_SUCCESS)//считываем данные с адресуемого устр-ва (пересылаем адрес устр-ва, 0 - сразу получить данные (без двойного старта), адрес регистра для записи ответа, 2 - кол-во ожидаемых байт в ответе)
//        {
//          state_I2C.PART.LM75_online=true;
//        }
        else
        {
            state_I2C.PART.LM75_online=false;
            Temp_Hg[0]=0xFF;
            Temp_Hg[1]=0xFF;
        }
//        if(i2c_rd_reg(0x44,0x21,0x30,&Temp_Hg[0],6)==I2C_SUCCESS)//считываем данные с адресуемого устр-ва (пересылаем адрес устр-ва, 0 - сразу получить данные (без двойного старта), адрес регистра для записи ответа, 2 - кол-во ожидаемых байт в ответе)
//        {
//          state_I2C.PART.SHT30_online=true;
//        }
//        else
//        {
//            state_I2C.PART.SHT30_online=false;
//            Temp_Hg[0]=0xFF;
//            Temp_Hg[1]=0xFF;
//        }
    }
}
//============================================================
void ctrl_klava (void)      //проверка кнопок
{
    if(ctrl_kn(press_adress)==true);	//проверяем сработала ли кнопка 
    {
        if(sost_kn[press_adress].PART.kn_ready_short==true)
        {
            sost_kn[press_adress].PART.kn_ready_short=false;
            short_press_adress();     //обработка короткого нажатия кнопки "адрес"
        }
        else if(sost_kn[press_adress].PART.kn_ready_long==true)
        {
            sost_kn[press_adress].PART.kn_ready_long=false;
            long_press_adress();     //обработка длинного нажатия кнопки "адрес"
        }
    }
 }

//--------------------------------------------------------------
void short_press_adress (void)	//обработка обычного нажатия кнопки адрес
{
    clr_my_adress();             //очистка собственного адреса
    FL_WAIT_ADDR=true;          //уст. флага ожидания получения нового алреса от мастера
    eeprom_write(O_type_sensor,0);    //обнуление типа подключенных датчиков без-ти
}
//--------------------------------------------------------------------
void long_press_adress (void)	//обработка длинного нажатия кнопки адрес
{
}
//===============================================================
void med_V_I(void)		//подпрограмма усреднения выбр каналов
{
  uint8_t i;
    static uint32_t
    ADC_OUT_OLD[3];//пред измеренное значение входов АЦП
    for(i=0; i<3; i++)
    {
        IND_NEW = &ADC_OUT[i];//запоминаем адреса обрабатываемых переменных
	IND_OLD = &ADC_OUT_OLD[i];
	medium();		//запускаем функцию усреднения
    }
}
//===============================================================
void ctrl_IN (uint8_t num_IN, uint8_t ADC_IN)        //формирование состояния входов датчиков в зав-ти от напряжения на входе  
{
  if((ADC_IN<ADC_change_down[num_IN])||(ADC_IN>ADC_change_up[num_IN]))
  {
    current_state_IN[num_IN].PART.open=false;
    current_state_IN[num_IN].PART.KZ=false;
    current_state_IN[num_IN].PART.change=true;
  }
  else if((ADC_IN>=ADC_open_down)&&(ADC_IN<=ADC_open_up))
  {
    current_state_IN[num_IN].PART.open=true;
    current_state_IN[num_IN].PART.KZ=false;
    current_state_IN[num_IN].PART.change=false;
  }
  else if((ADC_IN>=ADC_KZ_down)&&(ADC_IN<=ADC_KZ_up))
  {
    current_state_IN[num_IN].PART.open=false;
    current_state_IN[num_IN].PART.KZ=true;
    current_state_IN[num_IN].PART.change=false;
  }
  else 
  {
    current_state_IN[num_IN].PART.open=false;
    current_state_IN[num_IN].PART.KZ=false;
    current_state_IN[num_IN].PART.change=false;
  }
}
//===============================================================
void stable_state_IN(uint8_t num_IN)                     //ожидание стабилизации состояния 
{
  static uint8_t count_izmer[3];
  if(buffer_state_IN[num_IN].ALL==current_state_IN[num_IN].ALL)
  {
    count_izmer[num_IN]++;
    if(count_izmer[num_IN]>=O_stable_sost)
    {
      if((actual_state_IN[num_IN].PART.change==false)&&(current_state_IN[num_IN].PART.change==true))
      {
        count_izmen_input[num_IN]++;
      }
      actual_state_IN[num_IN].ALL=current_state_IN[num_IN].ALL;
      count_izmer[num_IN]=0;
    }
  }
  else
  {
    buffer_state_IN[num_IN].ALL=current_state_IN[num_IN].ALL;
    count_izmer[num_IN]=0;
  }
}
//===============================================================
void output_state_formation(uint8_t num_IN)     //формирование выходного байта с состоянием 2-х входов АЦП (где распаяны датчики без-ти)
{
  current_state_input_ADC.PART.KZ_chan1=actual_state_IN[0].PART.KZ;
  current_state_input_ADC.PART.change_chan1=actual_state_IN[0].PART.change;
  current_state_input_ADC.PART.open_chan1=actual_state_IN[0].PART.open;

  current_state_input_ADC.PART.KZ_chan2=actual_state_IN[1].PART.KZ;
  current_state_input_ADC.PART.change_chan2=actual_state_IN[1].PART.change;
  current_state_input_ADC.PART.open_chan2=actual_state_IN[1].PART.open;
}

