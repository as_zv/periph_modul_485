/* 
 * File:   my_key.h
 * Author: Anton
 *
 * Created on 12 июль 2017 г., 14:42
 */

#ifndef MY_KEY_PRESS_H
#define	MY_KEY_PRESS_H

#include "hardware.h"   //константы и функции привязанные к конкретному контроллеру

#define press_adress 1

bool key_press(uint8_t num_kn);		//
#endif	/* MY_KEY_PRESS_H */

