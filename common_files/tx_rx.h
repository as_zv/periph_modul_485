/* 
 * File:   tx_rx.h
 * Author: Anton
 *  файл описания функций для отправки и приёма данных
 * 
 * Created on 7 Май 2016 г., 14:42
 */

#ifndef TX_RX_H
#define	TX_RX_H

//#include <xc.h> 		//глобальные хидеры (в самом компиляторе)
#include <stdio.h>
#include "stdbool.h"
//#include <stdint.h>
#include "stdlib.h"
#include "string.h"
#include "hardware.h"   //константы и функции привязанные к конкретному контроллеру
#include "short_func.h" //описания коротких функций выполняемых по командам управления

#define peredacha 1
#define priem 0


//переменные
extern uint8_t
MY_ADRESS,              //ячейка для записи собственного адреса
NUM_BYTE;               //номер текущего принятого байта в посылке

extern bool 
PACKET_RX_YES,          //флаг для основной программы об успешном принятии посылки из линии 
timeout_tx;

#define SIZE_XD 33      //максимальный размер посылки с данными посылки (включая 1 байт CRC) (ограничение по таймеру 83 байта) 
#define SIZE_SHORT_SEND 6//размер короткой посылки

extern const uint8_t 
ADDR_KOMP,              //адрес зарезервирован для компьютера (для отладки и прошивки)
ADDR_CTRL_CENTR,        //центр контроля
ADDR_NEW_DEV,           //адрес нового устройтсва (которому ещё не присвоили адрес),
ADDR_ALL_DEV,           //адрес для широковещательных посылок
TOKEN1_MASTER,
TOKEN1_SLAVE,
TOKEN2;

struct fl_485
{
    unsigned RX_READY   : 1;//принят очередной байт с усарта
    unsigned BIG_PACKET : 1;//флаг означающий что принимаемая посылка длинная (содержит данные)
};
extern struct fl_485 FL_485;

typedef struct
packet_s
{                       
    uint8_t TOKEN1;
    uint8_t TOKEN2;
    uint8_t TO    ;
    uint8_t FROM  ;
    uint8_t FUNC  ;
    uint8_t HCRC  ;
    uint8_t SIZE  ;
    uint8_t XD_BYTE [SIZE_XD];
}
packet_t;

union u_p
{
	unsigned ALL_PACKET;
	packet_t PART;
};
extern union u_p RX_PACKET;

extern union u_p TX_PACKET;

//================================================================
void form_packet (uint8_t ADDR_TO, uint8_t FUNC, uint8_t SIZE_DATA);  //формирование пакета для передачи в линию
uint8_t Crc8(uint8_t *pcBlock, uint8_t len);//расчёт CRC
bool wait_reply(void);                  //ожидание ответа от опрашиваемого устр-ва
bool analiz_otveta(uint8_t ADDR, uint8_t FUNC); //проверка ответа на соответсвие требуемым параметрам
bool reply(uint8_t RX_NEW_BYTE);	//приём очередных байтов посылки
void TX(uint8_t DATA);                  //отправка данных в порт
void tx_full_send(void);                //отправка посылки с данными
void tx_short_send(void);               //отправка короткой посылки
void send_uart_program_success (uint8_t addr_device);//выдача сообщения об успешной прошивке, адреса девайса и прошитой версии прошивки 
void uart_puts(const uint8_t * s);     //отправка послания в линию
void uart_putch(uint8_t c);             //отправка одной переменной в UART
void start_new_rx_packet(void);         //запуск сборки нового пакета из линии 
void clr_RX(void);                      //очистка приёмника новых байт из линии и всё что с ним связано
void end_new_rx_packet(void);           //завершение сборки нового пакета из линии 

#endif	/* TX_RX_H */
