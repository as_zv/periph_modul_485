#include <iostm8s003f3.h>
#include "lm75.h"

// STM8S103F3P
// PB5 - 11 in TSSOP20 - SDA
// PB4 - 12 in TSSOP20 - SCL

// STM8S-Discovery
// PE2 - CN4.2 - SDA
// PE1 - CN4.3 - SCL

bool over_time_I2C;

void LM75_init()
{
  CLK_PCKENR1 |= 1;     // Разрешаем тактирование I2C

  I2C_CR1     = 0;
  I2C_FREQR   = 16;
  I2C_TRISER  = 16 + 1;
  I2C_CCRH    = 0;
  I2C_CCRL    = 0x50;
  I2C_CR1     = 1;
  I2C_OARL    = 0;
  I2C_OARH    = 0x40;
  I2C_CR2     = 0x04;
}

signed short LM75_read(unsigned char num, bool *pok)
{
  over_time_I2C=false; //засекаем 1мсек на обработку датчика (счётчик в прерывании установит этот флаг в "1" через 1 мсек)
  if(pok) *pok = 0;
  I2C_CR2_bit.START = 1;
  while((!(I2C_SR1 & MASK_I2C_SR1_SB))&&(over_time_I2C==false) );//добавлен контроль флага отсутствия обмена в течение 1мсек
  if(over_time_I2C==true)
     return 0; 
     I2C_DR      = 0x91 | ((num<<1)&0x0E);
  // RM0016 p.285 Read 2 bytes sequence
  // In Case of two bytes to be received:
  // - Set POS and ACK
  I2C_CR2_bit.ACK   = 1;
  I2C_CR2_bit.POS   = 1;
  // - Wait for the ADDR flag to be set
  while(!(I2C_SR1 & MASK_I2C_SR1_ADDR) ) 
  {
    if(I2C_SR2 & MASK_I2C_SR2_AF) 
    {
      I2C_SR2_bit.AF    = 0;
      I2C_CR2_bit.STOP  = 1;
      return 0;
    }
  }
  // - Clear ADDR
  I2C_SR3;
  // - Clear ACK
  I2C_CR2_bit.ACK   = 0;
  // - Wait for BTF to be set
  while(!(I2C_SR1 & MASK_I2C_SR1_BTF) );
  // - Program STOP
  I2C_CR2_bit.STOP  = 1;
  // - Read DR twice
  signed short temp;
  temp  = (I2C_DR) << 8;
  temp |= (I2C_DR)&0xE0;
  if(pok) *pok = 1;
  return temp;
}


