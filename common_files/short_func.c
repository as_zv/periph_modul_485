/*
 * File:   short_func.c
 * Author: Anton
 * файл коротких функций (команд присланых по интерфейсу управления)
 * Created on 5 марта 2017 г., 8:46
 */

#include "short_func.h" //описания коротких функций выполняемых по командам управления

//==== ПЕРЕМЕННЫЕ ============================================
bool energy_saving;   //включение энергосберегающего режимема 

//работа с EEPROM 
//адресация еепром
const uint8_t 
O_regim_starta = 0,         //адрес в eeprom значение которого определяет режим старта программы (прошивка или другое)
O_dev_adress = 1,             //адрес в eeprom значение которого определяет адрес устройства в сети
O_end_adress = 2,             //адрес в еепром значение которого определяет общее кол-во занятых адресов 
O_type_device_lo=3,              //адрес в еепром значение которого определяет подтип универсального устр-ва
O_type_sensor =4,              //адрес в еепром значение которого определяет тип подключенных датчиков без-ти

O_ADC1=10,
O_ADC2=11,
O_ADC3=12,

//значения в ячейке EEPROM: "режим старта программы":
O_progr_new_firmware = 1,   //старт програмирования новой прошивки
O_new_firmware_end = 2,     //новая прошивка успешно прошита
O_start_factory_firmware = 3,//старт с заводской прошивки, 
O_start_new_firmware = 4;   //старт с новой прошивки,

//----------------------------------------------------------------------
//объединения
union u_num_chan CCP_ready;

//=========================================================
void f_short_answer_on_command(uint8_t type_short_answer)   //посылка короткого ответа на команды от ЦП с соотв. номером функции 
{
    form_packet(RX_PACKET.PART.FROM,type_short_answer,0);
}
//============================================================
void f_answer_read_eeprom(uint8_t NUM_BYTE_START_READ, uint8_t COUNT_BYTE_READ)  //требуется ответить содержимым EEPROM                                                                   
{
	uint8_t I;
    uint8_t I_TX=2;
    for (I = NUM_BYTE_START_READ; I!=COUNT_BYTE_READ; I++)
    {
        TX_PACKET.PART.XD_BYTE[I_TX]=eeprom_read(I);
        I_TX++;
        if(I_TX>=SIZE_XD)
            return;
    }
    f_short_answer_on_command(answer_read_eeprom);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_write_eeprom(uint8_t NUM_BYTE_START_WRITE, uint8_t COUNT_BYTE_WRITE)  //требуется записать новое содержимое в EEPROM
                                        //(начальный байт адреса содержиться в XD_BYTE[0])
                                        //(кол-во байт содержиться в XD_BYTE[1])
{
	uint8_t I;
    uint8_t I_RX=2;
    for (I = NUM_BYTE_START_WRITE; I!=COUNT_BYTE_WRITE; I++)
    {
        eeprom_write(I,RX_PACKET.PART.XD_BYTE[I_RX]);
        I_RX++;
        if(I_RX>=SIZE_XD)
            return;
    }
    f_short_answer_on_command(answer_write_eeprom);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
 }
//--------------------------------------------------------------
void f_answer_zero_eeprom(void)//полное стирание EEPROM
{
    unsigned int I;
    for (I = 0; I!=all_eeprom_addr; I++)
    {
        eeprom_write(I,0x00);
    }

    f_short_answer_on_command(answer_zero_eeprom);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_start(void)           //запуск устройства
{
    
}
//--------------------------------------------------------------
void f_answer_stop(void)            //остановка устройства
{
    
}
//--------------------------------------------------------------
void f_answer_reset(void)           //сброс устройства
{
//    f_short_answer_on_command(answer_reset);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
//    Reset();
}
//--------------------------------------------------------------
void f_answer_old_firmware(void)//переход на заводскую прошивку
{
//    uint8_t temp;
//    eeprom_write(O_regim_starta,O_start_factory_firmware);
//    temp=eeprom_read(O_regim_starta);
//    if(temp==O_start_factory_firmware)
//    {
//        f_short_answer_on_command(answer_old_firmware);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
////        Reset();
//    }
//    else
//        f_short_answer_on_command(answer_error);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_new_firmware(void)    //переход на новую прошивку
{
//    uint8_t temp;
//    eeprom_write(O_regim_starta,O_start_new_firmware);
//    temp=eeprom_read(O_regim_starta);
//    if(temp==O_start_new_firmware)
//    {
//        f_short_answer_on_command(answer_new_firmware);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
////        Reset();
//    }
//    else
//        f_short_answer_on_command(answer_error);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_factory_param(void)//ответ на запрос текущей версии устройства
{
    uint8_t i=0;
    TX_PACKET.PART.XD_BYTE[i++]=sost_device[MY_ADRESS].PART.type_device_Hi;
    TX_PACKET.PART.XD_BYTE[i++]=sost_device[MY_ADRESS].PART.type_device_lo;
    TX_PACKET.PART.XD_BYTE[i++]=sost_device[MY_ADRESS].PART.type_sensor;
    TX_PACKET.PART.XD_BYTE[i++]=sost_device[MY_ADRESS].PART.ver_h;
    TX_PACKET.PART.XD_BYTE[i++]=sost_device[MY_ADRESS].PART.ver_l;
    TX_PACKET.PART.XD_BYTE[i++]=sost_device[MY_ADRESS].PART.count_byte_answer;
    form_packet(RX_PACKET.PART.FROM,answer_factory_param,i);
}
//--------------------------------------------------------------
void f_answer_progr(void)           //посылка включает на выбранном устройстве режим его прошивки
{
//    uint8_t temp;
//    eeprom_write(O_regim_starta,O_progr_new_firmware);
//    temp=eeprom_read(O_regim_starta);
//    if(temp==O_progr_new_firmware)
//    {
//        f_short_answer_on_command(answer_progr);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
////        Reset();
//    }
//    else
//        f_short_answer_on_command(answer_error);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_silence_on(void)         //переводит все устройства в режим тишины на линии для активации режима программирования
{
    FL_SILENCE_ON=true;
    TMR_SILENCE=0;
    f_short_answer_on_command(answer_silence_on);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_silence_off(void)         //выводит все устройства из режима тишины на линии 
{
    FL_SILENCE_ON=false;
    TMR_SILENCE=0;
    f_short_answer_on_command(answer_silence_off);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_blink_on(void)                   //включить мигание зелёного светика - для поиска устройства с определённым адресом
{
    FL_BLINK_ON=true;
    f_short_answer_on_command(answer_blink_on);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_blink_off(void)                   //выключить мигание зелёного светика
{
    FL_BLINK_ON=false;
    f_short_answer_on_command(answer_blink_off);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_freq_pwm(void)                   //пакет посылает новые частоты для каналов ШИМ (макс м.б. 8 каналов, т.к. размер посылки 32 байта):
                                                //0 байт - номер порта на котором включен ШИМ
                                                //1-3 байт - значение частоты ШИМ для канала
                                                //4 байт - номер следующего порта на котором включен ШИМ
                                                //5-7 байт - значение частоты ШИМ для следующего канала
{
}
//--------------------------------------------------------------
uint8_t bitCounter4 (uint8_t N) 
{
    N = (N & 0x55) + ((N >> 1) & 0x55);
    N = (N & 0x33) + ((N >> 2) & 0x33);
    N = (N & 0x0F) + ((N >> 4) & 0x0F);
    return N;
}
//--------------------------------------------------------------
void f_answer_ust_func_pin(void)                //настройка режима работы выходов: 
//                                                //1байт - режим работы 1 выхода, 2байт - режим работы 2 выхода и т.д, если байт равен 0 - ничего не меняем 
//                                                //(побитовое определение (1 - включена выбранная функция)
//                                                //0бит - вх., 1бит - вых., 2бит - АЦП, 3бит - ЦАП, 4бит - ШИМ, 5бит -I2С, 6бит - SPI, 7бит - спец. (определяется внутр. прошивкой)//--------------------------------------------------------------
{
//    uint8_t i;
//    uint8_t temp;
//    for(i=0; i<=quantity_port; i++)
//    {
//        temp=description_port[i].ALL & RX_PACKET.PART.XD_BYTE[i];
//        if(bitCounter4(temp)==1)
//        {
//            if(ust_func_port[i].ALL=!temp)
//            {
//                new_func_port[i]=true;
//                ust_func_port[i].ALL=temp;
//            }
//        }
//        TX_PACKET.PART.XD_BYTE[i]=ust_func_port[i].ALL;
//    }
//    i++;
//    form_packet(RX_PACKET.PART.FROM,answer_ust_func_pin,i);
}
////--------------------------------------------------------------
void f_answer_available_func_pin(void)          //запрос доступных режимов работы портов:
//                                                //1байт - доступные режимы работы 1-го выхода, 2байт - доступные режимы работы 2-го выхода и т.д. 
//                                                //(побитовое определение доступных функций портов (1 - функция доступна, если все нули - порта нет физически) - 
//                                                //0бит - вх., 1бит - вых., 2бит - АЦП, 3бит - ЦАП, 4бит - ШИМ, 5бит -I2С, 6бит - SPI, 7бит - спец. (определяется внутр. прошивкой)
{
//    uint8_t i;
//    for(i=0; i<=quantity_port; i++)
//    {
//        TX_PACKET.PART.XD_BYTE[i]=description_port[i].ALL;
//    }
//    i++;
//    form_packet(RX_PACKET.PART.FROM,answer_available_func_pin,i);
}
////--------------------------------------------------------------
void f_answer_new_adress(void)//присвоить данному устройству новый адрес в сети
{
    if((FL_WAIT_ADDR==true)&&(MY_ADRESS==ADDR_NEW_DEV))     //если нажата кнопка получения адреса и собственный адрес равен константе новых устройств
    {
        eeprom_write(O_dev_adress,RX_PACKET.PART.XD_BYTE[0]);   //заносим в еепромку полученный в посылке адрес устр-ву  
        MY_ADRESS=RX_PACKET.PART.XD_BYTE[0];                    //сохранение собств. адреса для использования
        FL_WAIT_ADDR=false;                                     //сброс флага запроса нового адреса
        f_short_answer_on_command(answer_new_adress);           //посылка короткого ответа на команды от ЦП с соотв. номером функции 
    }    
}
//--------------------------------------------------------------
void f_answer_device_without_addr(void)                 //ответ на запрос устр-в без адреса
{
    if((FL_WAIT_ADDR==true)&&(MY_ADRESS==ADDR_NEW_DEV))     //если нажата кнопка получения адреса и собственный адрес равен константе новых устройств
    {
         f_short_answer_on_command(answer_device_without_addr);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
    }

}
//--------------------------------------------------------------
void f_answer_available(void)                  //посылка ответа на запрос о наличии выбранного устр-ва на линии
{
    f_short_answer_on_command(answer_available);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//--------------------------------------------------------------
void f_answer_new_param(void)                  //посылка подтверждения на установку новых параметров
{
    f_short_answer_on_command(answer_new_param);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//------------------------------------------------------------
void f_answer_energy_saving_on(void)                  //посылка ответа на команду перехода в энергосберегающий режим
{
    energy_saving=true;                                                //выключаем режим энергосбережения
    f_short_answer_on_command(answer_energy_saving_on);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//------------------------------------------------------------
void f_answer_energy_saving_off(void)                  //посылка ответа на команду перехода в энергосберегающий режим
{
    energy_saving=false;                                                //выключаем режим энергосбережения
    f_short_answer_on_command(answer_energy_saving_off);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
}
//------------------------------------------------------------
void f_answer_set_type_sensor(void)           //подтвержение сохранения в EEPROM подтипа устр-ва и типа подключенных датчиков без-ти (объёмники, герконы, пожарка...) 
{
    f_short_answer_on_command(answer_set_type_sensor);      //посылка короткого ответа на команды от ЦП с соотв. номером функции 
//    eeprom_write(O_type_device_lo,RX_PACKET.PART.XD_BYTE[0]);    //сохранение подтипа устр-ва
    eeprom_write(O_type_sensor,RX_PACKET.PART.XD_BYTE[0]);    //сохранение типа подключенных датчиков без-ти
}
//------------------------------------------------------------
bool f_ask_available(uint8_t ADDR)	//запрос о наличии выбранного устр-ва на линии
{
    form_packet(ADDR,ask_available,0);
    return func_rx_answer_from_device(ADDR,answer_available);
}
//------------------------------------------------------------
bool f_ask_factory_param(uint8_t ADDR)                   //запрос текущей версии и общих параметров устройства
{
    form_packet(ADDR,ask_factory_param,0);
    return func_rx_answer_from_device(ADDR,answer_factory_param);
}
//------------------------------------------------------------
bool f_ask_now_state(uint8_t ADDR)                   //запрос состояния устройства
{
    form_packet(ADDR,ask_now_state,0);
    return func_rx_answer_from_device(ADDR,answer_now_state);
}
//------------------------------------------------------------
bool func_rx_answer_from_device(uint8_t ADDR, uint8_t type_short_answer)  //приём ответа от опрашиваемого устойства
{
    bool ret_out = false;

    if(PACKET_RX_YES==true)	//ожидание ответа
    {
        PACKET_RX_YES=false;
        if(analiz_otveta(type_short_answer,ADDR)==true)
        {
            sost_device[ADDR].PART.rx_err=0;	//и сброс флага неудачи
            ret_out = true;
        }
        else
        {
            sost_device[ADDR].PART.count_rx_err++;
            sost_device[ADDR].PART.rx_err=1;
        }
    }
    else
    {
        sost_device[ADDR].PART.count_rx_err++;
        sost_device[ADDR].PART.rx_err=1;
    }
    if(ret_out==false)
    {
        PIN_SV_RED=true;
    }
    return ret_out;
}
//=====================================================================
uint8_t ctrl_priem (void)
{
    uint8_t ret_out = false;
    if(PACKET_RX_YES==true)//если пришла новая посылка
    {
        PACKET_RX_YES=false;
            if(analiz_otveta(any_func,any_adress)==true)    //проверяем только CRC (от кого посылка и её назначение может быть любым)
            {
                if(RX_PACKET.PART.FUNC == command_zero_eeprom)
                    f_answer_zero_eeprom();    //полное стирание EEPROM
                else if(RX_PACKET.PART.FUNC == command_start)
                    f_answer_start();           //запуск устройства
                else if(RX_PACKET.PART.FUNC == command_stop)
                    f_answer_stop();            //остановка устройства
                else if(RX_PACKET.PART.FUNC == command_reset)
                    f_answer_reset();           //сброс устройства
                else if(RX_PACKET.PART.FUNC == command_old_firmware)
                    f_answer_old_firmware();//переход на заводскую прошивку
                else if(RX_PACKET.PART.FUNC == command_new_firmware)
                    f_answer_new_firmware();    //переход на новую прошивку
                else if(RX_PACKET.PART.FUNC == command_progr)
                    f_answer_progr();           //включение на выбранном устройстве режима его прошивки
                else if(RX_PACKET.PART.FUNC == command_silence_on)
                    f_answer_silence_on();         //перевод устройства в режим тишины на линии для активации режима программирования
                else if(RX_PACKET.PART.FUNC == command_silence_off)
                    f_answer_silence_off();         //вывод устройства из режима тишины на линии в нормальный режим
                else if(RX_PACKET.PART.FUNC == command_new_adress)
                    f_answer_new_adress();//присвоить устройству полученный адрес
                else if(RX_PACKET.PART.FUNC == ask_device_without_addr)
                    f_answer_device_without_addr();//получен запрос на наличие устр-в без адреса с нажатой кнопкой ожидания адреса
                else if(RX_PACKET.PART.FUNC == command_ust_func_pin)
                    f_answer_ust_func_pin();//настройка режима работы выходов ПП
                else if(RX_PACKET.PART.FUNC == command_write_eeprom)
                    f_answer_write_eeprom(RX_PACKET.PART.XD_BYTE[0],RX_PACKET.PART.XD_BYTE[1]);//требуется записать новое содержимое в EEPROM
                else if(RX_PACKET.PART.FUNC == command_blink_on)
                    f_answer_blink_on();                    //включает мигание зелёным светиком для идентификации устройства по адресу
                else if(RX_PACKET.PART.FUNC == command_blink_off)
                    f_answer_blink_off();                    //выключает мигание зелёным светиком
                else if(RX_PACKET.PART.FUNC == command_freq_pwm)
                    f_answer_freq_pwm();                    //выключает мигание зелёным светиком
                else if(RX_PACKET.PART.FUNC == command_energy_saving_on)
                    f_answer_energy_saving_on();                    //включает режим экономии энергии (все мощные потребители блокируются)
                else if(RX_PACKET.PART.FUNC == command_energy_saving_off)
                    f_answer_energy_saving_off();                    //выключает режим экономии энергии (все мощные потребители начинают работать)
                else if(RX_PACKET.PART.FUNC == ask_read_eeprom)
                    f_answer_read_eeprom(RX_PACKET.PART.XD_BYTE[0],RX_PACKET.PART.XD_BYTE[1]);//требуется ответить содержимым EEPROM  
                else if(RX_PACKET.PART.FUNC == ask_available)
                    f_answer_available();//ответ на запрос о наличии данного устройства на линии
                else if(RX_PACKET.PART.FUNC == ask_factory_param)
                    f_answer_factory_param();      //требуется ответить заводскими параметрами устройства
                else if(RX_PACKET.PART.FUNC == ask_available_func_pin)
                    f_answer_available_func_pin();//ответить - какие доступны режимов работы у имеющихся портов
                ret_out=RX_PACKET.PART.FUNC;
            }
        
    }
    return ret_out ;
}
