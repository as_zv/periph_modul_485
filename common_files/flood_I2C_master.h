#ifndef _flood_I2C_master_H
#define _flood_I2C_master_H

#include <iostm8s003f3.h>
#include <stdio.h>
#include "stdbool.h"
#include "stdint.h"
#include  <intrinsics.h>
#include "hardware.h"


void i2c_master_init(unsigned long f_master_hz, unsigned long f_i2c_hz);
// Инициализация I2C интерфейса      
// f_master_hz - частота тактирования периферии Fmaster          
// f_i2c_hz - скорость передачи данных по I2C       

//Результат выполнения операции с i2c
typedef enum {
    I2C_SUCCESS = 0,
    I2C_TIMEOUT,
    I2C_ERROR
} t_i2c_status;

//Таймаут ожидания события I2C
static unsigned long int i2c_timeout;
 
//Задать таймаут в микросекундах
#define set_tmo_us(time)\
  i2c_timeout = (unsigned long int)(F_MASTER_MHZ * time)
 
//Задать таймаут в миллисекундах
#define set_tmo_ms(time)\
  i2c_timeout = (unsigned long int)(F_MASTER_MHZ * time * 1000)
 
#define tmo               i2c_timeout--
 
//Ожидание наступления события event
//в течении времени timeout в мс
#define wait_event(event, timeout) set_tmo_ms(timeout);\
                                   while(event && i2c_timeout);\
                                   if(!i2c_timeout) return I2C_TIMEOUT;

t_i2c_status i2c_rd_reg(uint8_t address, uint8_t reg_addr_h, uint8_t reg_addr_l,
                              uint8_t * data, uint8_t length);// Чтение регистра slave-устройства

t_i2c_status i2c_wr_reg(uint8_t address, uint8_t reg_addr,
                              uint8_t * data, uint8_t length);// Запись регистра slave-устройства
//extern bool over_time_I2C;
#endif  /*_I2C_H*/
