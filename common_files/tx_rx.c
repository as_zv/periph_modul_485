/*
 * File:   tx_rx2.c
 * Author: Anton
 * comment: фойл содержащий функции для отправки и приёма данных по сети 
 * Created on 22 декабря 2016 г., 19:12
 */

#include "tx_rx.h"     //процедуры приёма и передачи данных

//переменные
uint8_t
MY_ADRESS,              //ячейка для записи собственного адреса
NUM_BYTE;               //номер текущего принятого байта в посылке

bool 
PACKET_RX_YES=0,          //флаг для основной программы об успешном принятии посылки из линии 
timeout_tx;

const uint8_t 
ADDR_KOMP     	= 1,    //адрес зарезервирован для компьютера (для отладки и прошивки)
ADDR_CTRL_CENTR	= 2,    //центр контроля
ADDR_NEW_DEV   	= 0x00, //адрес нового устройтсва (которому ещё не присвоили адрес),
ADDR_ALL_DEV   	= 0xFF, //адрес для широковещательных посылок

TOKEN1_MASTER   = 0X55,
TOKEN1_SLAVE    = 0XAA,
TOKEN2          = 0xFF;

struct fl_485 FL_485;

union u_p RX_PACKET;

union u_p TX_PACKET;

/*
  Name  : CRC-8
  Poly  : 0x31    x^8 + x^5 + x^4 + 1
  Init  : 0xFF
  Revert: false
  XorOut: 0x00
  Check : 0xF7 ("123456789")
  MaxLen: 15 байт (127 бит) - обнаружение
    одинарных, двойных, тройных и всех нечетных ошибок
*/
const uint8_t Crc8Table[256] = {
    0x00, 0x31, 0x62, 0x53, 0xC4, 0xF5, 0xA6, 0x97,
    0xB9, 0x88, 0xDB, 0xEA, 0x7D, 0x4C, 0x1F, 0x2E,
    0x43, 0x72, 0x21, 0x10, 0x87, 0xB6, 0xE5, 0xD4,
    0xFA, 0xCB, 0x98, 0xA9, 0x3E, 0x0F, 0x5C, 0x6D,
    0x86, 0xB7, 0xE4, 0xD5, 0x42, 0x73, 0x20, 0x11,
    0x3F, 0x0E, 0x5D, 0x6C, 0xFB, 0xCA, 0x99, 0xA8,
    0xC5, 0xF4, 0xA7, 0x96, 0x01, 0x30, 0x63, 0x52,
    0x7C, 0x4D, 0x1E, 0x2F, 0xB8, 0x89, 0xDA, 0xEB,
    0x3D, 0x0C, 0x5F, 0x6E, 0xF9, 0xC8, 0x9B, 0xAA,
    0x84, 0xB5, 0xE6, 0xD7, 0x40, 0x71, 0x22, 0x13,
    0x7E, 0x4F, 0x1C, 0x2D, 0xBA, 0x8B, 0xD8, 0xE9,
    0xC7, 0xF6, 0xA5, 0x94, 0x03, 0x32, 0x61, 0x50,
    0xBB, 0x8A, 0xD9, 0xE8, 0x7F, 0x4E, 0x1D, 0x2C,
    0x02, 0x33, 0x60, 0x51, 0xC6, 0xF7, 0xA4, 0x95,
    0xF8, 0xC9, 0x9A, 0xAB, 0x3C, 0x0D, 0x5E, 0x6F,
    0x41, 0x70, 0x23, 0x12, 0x85, 0xB4, 0xE7, 0xD6,
    0x7A, 0x4B, 0x18, 0x29, 0xBE, 0x8F, 0xDC, 0xED,
    0xC3, 0xF2, 0xA1, 0x90, 0x07, 0x36, 0x65, 0x54,
    0x39, 0x08, 0x5B, 0x6A, 0xFD, 0xCC, 0x9F, 0xAE,
    0x80, 0xB1, 0xE2, 0xD3, 0x44, 0x75, 0x26, 0x17,
    0xFC, 0xCD, 0x9E, 0xAF, 0x38, 0x09, 0x5A, 0x6B,
    0x45, 0x74, 0x27, 0x16, 0x81, 0xB0, 0xE3, 0xD2,
    0xBF, 0x8E, 0xDD, 0xEC, 0x7B, 0x4A, 0x19, 0x28,
    0x06, 0x37, 0x64, 0x55, 0xC2, 0xF3, 0xA0, 0x91,
    0x47, 0x76, 0x25, 0x14, 0x83, 0xB2, 0xE1, 0xD0,
    0xFE, 0xCF, 0x9C, 0xAD, 0x3A, 0x0B, 0x58, 0x69,
    0x04, 0x35, 0x66, 0x57, 0xC0, 0xF1, 0xA2, 0x93,
    0xBD, 0x8C, 0xDF, 0xEE, 0x79, 0x48, 0x1B, 0x2A,
    0xC1, 0xF0, 0xA3, 0x92, 0x05, 0x34, 0x67, 0x56,
    0x78, 0x49, 0x1A, 0x2B, 0xBC, 0x8D, 0xDE, 0xEF,
    0x82, 0xB3, 0xE0, 0xD1, 0x46, 0x77, 0x24, 0x15,
    0x3B, 0x0A, 0x59, 0x68, 0xFF, 0xCE, 0x9D, 0xAC
};

uint8_t Crc8(uint8_t *pcBlock, uint8_t len)
{
    uint8_t crc = 0xFF;
 
    while (len--)
        crc = Crc8Table[crc ^ *pcBlock++];
 
    return crc;
}

void TX(uint8_t DATA)		//отправка данных
{
	while(TX1_IF==0);	//ожидание освобожденияпредыдущей передачи
	{
	}
	TX1_BUF=DATA;		//передача следующего байта
    while(TX1_END==0)	//ожидание окончания передачи 
	{               //переделать на проверку по окончанию общей передачи а не каждого байта
	}
}

void end_new_rx_packet(void) //завершение сборки нового пакета из линии 
{
    PACKET_RX_YES=true;   //уст. флаг готовности принятой посылки для последующего анализа 
    TMR1_ON=false;        //выключение таймера
    TMR1_IE=0;            //запрешаем прерывание от таймера при переполении счётчика
    TMR1_Lo=0;            //где O_TIM_BYTE_485 - кол-во тактов на 1 байт, 4 - мин. кол-во байт интервала (взято из modbus)
    TMR1_Hi=0;
    TMR1_IF=0;           //сброс флага таймера (если сработал)
    timeout_tx=true;    //уст. флаг запуска таймера ожидания между посылками для передатчика 
}

void start_new_rx_packet(void)            //запуск сборки нового пакета из линии
{
    while(TX1_END==0)//ожидание окончания передачи
        {
        }
    TMR1_ON=false;       //выключение таймера
    TMR1_Hi=0;
    TMR1_Lo=0;
    TMR1_IF=0;		//сброс флага таймера (если сработал)
    TMR1_ON=true;        //включение таймера
    TMR1_IE=1;  //разрешшаем прерывание от таймера при переполении счётчика
}

void clr_RX(void)               //очистка приёмника новых байт из линии и всё что с ним связано
{
    FL_485.RX_READY=0;  //сброс флага принятия байта из линии
    NUM_BYTE=0;
    memset(&RX_PACKET.PART.TOKEN1,0,(SIZE_SHORT_SEND+SIZE_XD));
    FL_485.BIG_PACKET=0;
}

bool reply(uint8_t RX_NEW_BYTE)	//приём очередных байтов посылки
{
    bool END_RX=false;
    FL_485.RX_READY=0;
    NUM_BYTE++;
    if(NUM_BYTE<=SIZE_SHORT_SEND)	//если большая посылка ещё не началась
    {
		switch (NUM_BYTE)	//записываем очередной принятый байт
        {					//в нужную ячейку
            case 1:
            {
                TMR1_END=((SIZE_SHORT_SEND+1)*O_TIM_BYTE_485);   //время ожидания посылки (где SIZE_SHORT_SEND - кол-во байт в первой половине посылки, O_TIM_BYTE_485 - кол-во тактов таймера на 1 байт, 1 - запасное время в 1 байт)
                RX_PACKET.PART.TOKEN1=RX_NEW_BYTE;
                if((RX_PACKET.PART.TOKEN1==TOKEN1_MASTER)||(RX_PACKET.PART.TOKEN1==TOKEN1_SLAVE))
                    break;
                else
                    clr_RX();
                break;
            }
            case 2:
            {
                RX_PACKET.PART.TOKEN2=RX_NEW_BYTE;
                if(TOKEN2==RX_PACKET.PART.TOKEN2)
                    break;
                else
                    clr_RX();
                break;
            }
            case 3:
            {
                RX_PACKET.PART.TO=RX_NEW_BYTE;	 
                if((RX_PACKET.PART.TO==MY_ADRESS)||(RX_PACKET.PART.TO==any_adress))
                    break;
                else
                    clr_RX();
                break;
            }
            case 4:
            {
                RX_PACKET.PART.FROM=RX_NEW_BYTE;	
                break;
            }
            case 5:
            {
                RX_PACKET.PART.FUNC=RX_NEW_BYTE;	
                break;
            }
            case 6:
            {
                RX_PACKET.PART.HCRC=RX_NEW_BYTE;	
                if(RX_PACKET.PART.FUNC<0x80)
                {
                    END_RX=true;	//флаг окончания посылки
                    RX_NEW_BYTE=0;
                    NUM_BYTE=0;
                    FL_485.BIG_PACKET=0;
                    break;
                }
                else
                    FL_485.BIG_PACKET=true;
                    break;
            }
            default:
            break;
	}
    }
    else if(NUM_BYTE==(SIZE_SHORT_SEND+1)&&(FL_485.BIG_PACKET==true))	//
	{
        RX_PACKET.PART.SIZE=RX_NEW_BYTE;	
        TMR1_END=TMR1_END+((3+RX_PACKET.PART.SIZE)*O_TIM_BYTE_485);   //время ожидания посылки (где SIZE_SHORT_SEND - кол-во байт в первой половине посылки, O_TIM_BYTE_485 - кол-во тактов таймера на 1 байт, 3: (1байт - размер, 1байт - CRC, 1байт - запасное время )
    }
    else if((NUM_BYTE>(SIZE_SHORT_SEND+1))&&(NUM_BYTE<=(SIZE_XD+(SIZE_SHORT_SEND+1)))&&(NUM_BYTE<=(RX_PACKET.PART.SIZE+1+(SIZE_SHORT_SEND+1)))&&(END_RX==false)) //если кол-во байт в пределах заданного размера
    {
        RX_PACKET.PART.XD_BYTE[(NUM_BYTE-1)-(SIZE_SHORT_SEND+1)]=RX_NEW_BYTE;//переносим принятый буфер в соотв. байт
    }
    if(NUM_BYTE==(RX_PACKET.PART.SIZE+1+(SIZE_SHORT_SEND+1)))   //проверяем размер посылки согласно присланному размеру (1 это байт CRC в посылке и ещё 1 байт размера посылки)
    {
        END_RX=true;	//флаг окончания посылки
        NUM_BYTE=0;
        FL_485.BIG_PACKET=0;
    }
    return END_RX;    
}
//======================================================
bool analiz_otveta(uint8_t type_answer, uint8_t ADDR)
{
    bool ret_out=0;
    if(RX_PACKET.PART.HCRC==Crc8(&RX_PACKET.PART.TOKEN1,SIZE_SHORT_SEND-1))
    {
        if((RX_PACKET.PART.FUNC==type_answer)||(type_answer==any_func))
        {
            if((RX_PACKET.PART.FROM==ADDR)||(ADDR==any_adress))
            {
                if((RX_PACKET.PART.TO==MY_ADRESS)||(RX_PACKET.PART.TO==ADDR_ALL_DEV))
                {
                    if(RX_PACKET.PART.SIZE==0)
                        ret_out=true;
                    else
                    {
                        if(RX_PACKET.PART.XD_BYTE[RX_PACKET.PART.SIZE] == Crc8(&RX_PACKET.PART.XD_BYTE[0],RX_PACKET.PART.SIZE))
                            ret_out=true;
                        else
                            ret_out=false;
                    }
                }
                else
                {
                    ret_out=false;
                }
            }
            else
            {
                ret_out=false;
            }
        }
        else
        {
            ret_out=false;
        }
    }
    else
    {
        ret_out=false;
    }
    if(ret_out==false)
    {
        PIN_SV_RED=true;
    }
    return ret_out;
}
//======================================================
void send_uart_program_success (uint8_t addr_device)
{
//    uint8_t buffer[3]; //результат
//    itoa(buffer, addr_device, 10);
//    uart_puts("program successfull device, adress=");
//    uart_puts(buffer);
//    uart_puts("\n\r version=");
//    itoa(buffer, ver_h, 10);
//    uart_puts(buffer);
//    uart_puts(".");
//    itoa(buffer, ver_l, 10);
//    uart_puts(buffer);
//    eeprom_write(O_regim_starta,O_new_firmware_end);//новая прошивка успешно прошита
}
//======================================================
void uart_puts(const uint8_t * s)//отправка строки в линию UART
{
	RX1_ON=0;	//выключение приёмника
	TX1_ON=1;	//включение передатчика
	upr_485(peredacha);		//переключение драйвера на передачу
	while(*s)
        TX(*s++);
        TX1_ON=0;	//выключение передатчика
	RX1_ON=1;	//включение приёмника
	upr_485(priem);		//переключение драйвера на приём
}
//=======================================================
void uart_putch(uint8_t c)//отправка одной переменной в UART
{
	RX1_ON=0;	//выключение приёмника
	TX1_ON=1;	//включение передатчика
	upr_485(peredacha);		//переключение драйвера на передачу
        TX(c);
        TX1_ON=0;	//выключение передатчика
	RX1_ON=1;	//включение приёмника
	upr_485(priem);		//переключение драйвера на приём
}
//------------------------------------------------------------
void short_send(void)   //передача данных в порт
{
//    TX(0xFF);                   //сброс предыдущих помех (для сложных условий)
    TX(TX_PACKET.PART.TOKEN1);
    TX(TX_PACKET.PART.TOKEN2);
    TX(TX_PACKET.PART.TO);
    TX(TX_PACKET.PART.FROM);
    TX(TX_PACKET.PART.FUNC);
    TX(TX_PACKET.PART.HCRC);
}
//=======================================================
void tx_short_send(void)	//отправка короткой посылки
{
	RX1_ON=0;	//выключение приёмника
	TX1_ON=1;	//включение передатчика
	upr_485(peredacha);		//переключение драйвера на передачу
	short_send();       //посылаем короткую посылку
        TX1_ON=0;	//выключение передатчика
	RX1_ON=1;	//включение приёмника
	upr_485(priem);		//переключение драйвера на приём
}
//------------------------------------------------------------
void tx_full_send(void)	//отправка полной посылки
{
    uint8_t i;
	RX1_ON=0;	//выключение приёмника
	TX1_ON=1;	//включение передатчика
	upr_485(peredacha);		//переключение драйвера на передачу
	short_send();       //посылаем короткую посылку
    if(TX_PACKET.PART.SIZE!=0)
    {
        TX(TX_PACKET.PART.SIZE);
        for(i =0;i<=(TX_PACKET.PART.SIZE);i++)	//ждём окончания передачи
        {
            TX(TX_PACKET.PART.XD_BYTE[i]);
        }
    }
    TX1_ON=0;	//выключение передатчика
	RX1_ON=1;	//включение приёмника
	upr_485(priem);		//переключение драйвера на приём
}
//------------------------------------------------------------
void form_packet (uint8_t ADDR_TO,uint8_t FUNC,uint8_t SIZE_DATA)  //формирование пакета для передачи в линию
{
    if(RX_PACKET.PART.TO!=any_adress)
    {
        if((MY_ADRESS==ADDR_CTRL_CENTR)||(MY_ADRESS==ADDR_KOMP))
            TX_PACKET.PART.TOKEN1=TOKEN1_MASTER;
        else
            TX_PACKET.PART.TOKEN1=TOKEN1_SLAVE;
        TX_PACKET.PART.TOKEN2=TOKEN2;
        TX_PACKET.PART.TO=ADDR_TO;//адрес назначения посылки
        TX_PACKET.PART.FROM=MY_ADRESS;	//собственный адрес (от кого ответ)
        TX_PACKET.PART.FUNC=FUNC;//передача новых данных
        TX_PACKET.PART.HCRC = Crc8(&TX_PACKET.PART.TOKEN1,SIZE_SHORT_SEND-1);
        while((timeout_tx==true)&&(TMR1_ON==true)) //проверяем флаг таймера ожидания между посылками 
        {
            while((TMR1_IF==false)&&(TMR1_ON==true))
            {
            }
            timeout_tx=false;
            TMR1_IF=false;		//сброс флага таймера (если сработал)
            TMR1_ON=false;	//выключение таймера
	}
        if(SIZE_DATA==0)
        {
           tx_short_send();
        }
        else
        {
            TX_PACKET.PART.SIZE=SIZE_DATA;     //кол-во байт в пакете
            TX_PACKET.PART.XD_BYTE[SIZE_DATA++]= Crc8(&TX_PACKET.PART.XD_BYTE[0],TX_PACKET.PART.SIZE); // расчёт CRC для посылки
            tx_full_send();
        }
        timeout_tx=true;     //уст. флаг запуска таймера ожидания между посылками для передатчика 
       	TMR1_ON=false;       	//выключение таймера
    	TMR1_Lo=0;            //где O_TIM_BYTE_485 - кол-во тактов на 1 байт, byte_timout_tx_rx - мин. кол-во байт интервала для последующего приёма (взято из modbus)
        TMR1_IF=0;           //сброс флага таймера (если сработал)
        TMR1_END=(O_TIM_BYTE_485 * byte_timout_tx_rx); //задаём таймер интервала между посылками + 1,5 байта уходит на программные издержки
       	TMR1_ON=true;        //включение таймера
    }
}

