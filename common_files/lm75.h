#ifndef _LM75_H
#define _LM75_H

#include "stdbool.h"
#include "stdint.h"
#include "interrupt.h"  //файл описания прерываний

void LM75_init();
signed short LM75_read(unsigned char num, bool *pok);
#endif  /*_LM75_H*/
