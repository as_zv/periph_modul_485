/*
 * File:   my_klava.c
 * Author: Anton
 * comment: 
 * Created on october 24, 2017, 9:42 PM
 */

#include "my_klava.h"

const uint8_t 
O_UP_DOWN = 40,           //для увел. значения при удержании кнопки 0,4с
O_DREBEZG = 20,           //для дребезга 0,2с
O_KN = 60,         //др. функция кнопки при нажатии более 0,6с
O_COUNT_KN = 3;     //счётчик минимального удержания кнопки нажатой для запуска обработки (кол-во сработок подряд)

//---------------------------------------------------------------
//описание структур
union u_sost_kn sost_kn[quantity_kn];       //общий массив состояния кнопок
//==============================================================
void klava(uint8_t num_kn)		//программа обработки кнопок
{
	sost_kn[num_kn].PART.kn_press=(key_press(num_kn));	//запускаем прогу обработки нажатия кнопки
	if(sost_kn[num_kn].PART.kn_press!=sost_kn[num_kn].PART.kn_press_copy) //проверяем этот опрос совпадает с пред. опросом
	{
		sost_kn[num_kn].PART.kn_press_copy=sost_kn[num_kn].PART.kn_press; //если нет, запоминаем новое состояние кнопок
        sost_kn[num_kn].PART.count_kn=0;     //и сброс счётчика удержания кнопки нажатой
		return;
	}
	else
	{
        sost_kn[num_kn].PART.count_kn++;         //увел. счётчик удержания кнопки
        if(sost_kn[num_kn].PART.count_kn!=O_COUNT_KN)//до заданного времени
            return;
        sost_kn[num_kn].PART.count_kn=0;
		sost_kn[num_kn].PART.kn_press_copy=0;
        if(sost_kn[num_kn].PART.kn_press==0)   //проверяем нажатие на кнопку
        {
            sost_kn[num_kn].PART.kn_press_long=0;
			return;         //нет
        }
        sost_kn[num_kn].PART.kn_drebezg_tmr=0;		//если  кнопки сработали то начинаем отсчёт заново
		if(sost_kn[num_kn].PART.kn_drebezg_yes==0)	//разрешёно срабатывние кнопок? (дребезг прошёл?)
			return;         //нет
        if(sost_kn[num_kn].PART.kn_press==1)   //проверяем нажатие на кнопку
        {
            sost_kn[num_kn].PART.kn_press_long=1;//да, засекаем таймер на длит. нажатие кнопки
        }
        else
        {
             sost_kn[num_kn].PART.kn_press_long=0;//нет, засекаем таймер на длит. нажатие кнопки
        }
	}
}
bool ctrl_kn (uint8_t num_kn)	//контроль длительности нажатия кнопки
{
//--------- обязательная часть ------
    bool ret_out;
    ctrl_tim_drebezg(num_kn); //проверка дребезга кнопок
    klava(num_kn);
	if((sost_kn[num_kn].PART.kn_press_long==0)&&(sost_kn[num_kn].PART.kn_press_short==0))//было нажатие кнопки?
       ret_out=false;
	else if((sost_kn[num_kn].PART.kn_press_long==0)&&(sost_kn[num_kn].PART.kn_press_short==1))//короткое нажатие кнопки?
	{
        sost_kn[num_kn].PART.kn_press_short=0;	//сброс что было короткое нажатие для след. раза
        sost_kn[num_kn].PART.kn_press_long=0;	//сброс что была попытка длит. нажатия
        sost_kn[num_kn].PART.kn_long_tmr=0;	//очищаем рабочие ячейки для след раза
        sost_kn[num_kn].PART.kn_drebezg_yes=0;            //запрещаем включение прерываний 
    //--------- исполняемый код при коротком нажатии кнопки ------
        sost_kn[num_kn].PART.kn_ready_short=1;
		ret_out=true;
	}
    else
    {
        sost_kn[num_kn].PART.kn_press_short=1;		//запоминаем что только что было нажатие
        sost_kn[num_kn].PART.kn_long_tmr++;		//увел время прошедшее после нажатия кнопки
    }
    if(sost_kn[num_kn].PART.kn_long_tmr==O_KN)	//проверяем пора включить другую функцию?
    {
        sost_kn[num_kn].PART.kn_press_short=0;	//сброс что было короткое нажатие для след. раза
        sost_kn[num_kn].PART.kn_long_tmr=0;
        sost_kn[num_kn].PART.kn_press_long=0;	//сброс что была попытка длит. нажатия
        sost_kn[num_kn].PART.kn_drebezg_yes=0;            //запрещаем включение прерываний 
//--------- исполняемый код при удержании кнопки ------
		sost_kn[num_kn].PART.kn_ready_long=1;
		ret_out=true;
    }
    else
		ret_out=false;
    
    if((sost_kn[num_kn].PART.kn_ready_long==1)&&(sost_kn[num_kn].PART.kn_press==1))
    {
        sost_kn[num_kn].PART.kn_up_down_tmr++;//да
        while (sost_kn[num_kn].PART.kn_up_down_tmr==O_UP_DOWN)
        {
            sost_kn[num_kn].PART.kn_up_down_tmr=0; //время прошло,
            sost_kn[num_kn].PART.kn_increment=1;     //вкл. функции увел(уменьш.) значения
        }
    }
    else if((sost_kn[num_kn].PART.kn_ready_long==1)&&(sost_kn[num_kn].PART.kn_press==0)&&(sost_kn[num_kn].PART.kn_drebezg_yes==1))
    {
            sost_kn[num_kn].PART.kn_up_down_tmr=0; //время прошло,
            sost_kn[num_kn].PART.kn_ready_long=0;
    }
    return ret_out;
}
void ctrl_tim_drebezg (uint8_t num_kn)    //проверка таймера кнопок
{            
    if(sost_kn[num_kn].PART.kn_drebezg_yes==0)//требуется ли засечь таймер для кнопок
    {
        sost_kn[num_kn].PART.kn_drebezg_tmr++;//да
        while (sost_kn[num_kn].PART.kn_drebezg_tmr==O_DREBEZG)
        {
            sost_kn[num_kn].PART.kn_drebezg_tmr=0; //время прошло,
            sost_kn[num_kn].PART.kn_drebezg_yes=1;//разрешаем кнопки
        }
    }
}
