/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef COMMON_FUNC_H
#define	COMMON_FUNC_H

//#include <xc.h> // include processor files - each processor file is guarded.  
#include "stdio.h"
#include "stdbool.h"
//#include "stdint.h"
#include "stdlib.h"
#include "tx_rx.h"
#include "hardware.h"
#include "short_func.h" //описания коротких функций выполняемых по командам управления
//-----------------------------------------------
//общие флаги для всех устройств
extern bool 
FL_TIM_SEC,         //сработал таймер 1 сек.
FL_TIM_1MSEC,      //сработал таймер 1 мсек.
FL_TIM_10MSEC,      //сработал таймер 10 мсек.
FL_TIM_100MSEC,      //сработал таймер 100 мсек.
FL_TIM_500MSEC,      //сработал таймер 500 мсек.
FL_TIM_NEW_DIGIT,    //сработал таймер обновления инфы на индикаторе (перекл. на след разряд)
FL_SILENCE_ON,     //флаг означающий включения режима тишины
FL_WAIT_ADDR,       //включен режим ожидания получения нового адреса от ЦП
FL_MIG_GREEN,       //включен режим мигания зелёного светика       
FL_MIG_RED,         //включен режим мигания красного светика  
FL_BLINK_ON,        //включение мигания светиком на устр-ве для его идентификации
FL_NEW_DAY_KORR_TIM,//требуется скорректировать время для программы часов (раз в сутки)
FL_KORR_TIM_DEC;    //требуется корректировка времени назад (чтобы не прибавилась лишняя минута)
//-----------------------------------------------
//общие переменные
extern uint8_t 
TMR_1MSEC,     //счётчик таймера для формирования 1мсек
TMR_10MSEC,     //счётчик таймера для формирования 10мсек
TMR_100MSEC,     //счётчик таймера для формирования 100мсек
TMR_500MSEC,    //счётчик таймера для формирования 500мсек
TMR_NEW_DIGIT,  //счётчик таймера для обновления следующего разряда семисегментного индикатора
SEC,
MIN,
HOUR,
DAY_WEEK;

extern uint16_t 
TMR_SEC,       //счётчик таймера для формирования 1сек
TMR_SILENCE;  //таймер времени тишины на линии (обычно для программирования)

//-----------------------------------------------
//КОНСТАНТЫ
extern const uint16_t O_TMR_SILENCE;    //время через которое сброситься режим тишины (5мин.)

//-----------------------------------------------
//УКАЗАТАЛИ
extern uint16_t *IND_NEW;	//новое измеренное значение
extern uint32_t *IND_OLD;	//предыдущее измеренное значение


//-----------------------------------------------
//объединения
typedef struct time
{
	uint8_t SEC			;//секунда события
	uint8_t MIN			;//минута события
	uint8_t HOUR		;//час события
	uint8_t DAY			;//день события
	uint8_t EVENT		;//произошедшее событие

}ttime;
union u_time
{
	unsigned ALL;
	ttime PART ;
};
extern union u_time TIME_EVENT;

//-----------------------------------------------
//описания функций

void bin_bcd4(uint16_t BIN);        //прога двоично-десятичного(4) преобразования
void bin_bcd3(uint16_t BIN);        //прога двоично-десятичного(3) преобразования
void bin_bcd2(uint8_t BIN);         //прога двоично-десятичного(2) преобразования
void count_time(void);              //программа подсчёта текущего времени
void clr_my_adress(void);           //очистка собственного адреса
void process_svetik_common (void);  //обработка светодиодов общих для всех
void ctrl_silence(void);            //проверка режима тишины
void medium(void);                  //программа устреднения показаний с АЦП

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

